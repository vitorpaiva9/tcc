﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Enums
{
    public enum Roles
    {
        Admin = 1,
        User
    }
    public enum StakeholderType
    {
        //[Display(Name = "Programmer")]
        [Display(Name = "Programador")]
        Programmer = 1,
        //[Display(Name = "Requirements Analist")]
        [Display(Name = "Analista de Requisitos")]
        RequirementsAnalist,
        //[Display(Name = "SoftwareArchitect")]
        [Display(Name = "Arquiteto de Software")]
        SoftwareArchitect,
        //[Display(Name = "Quality Assurance Engineer")]
        [Display(Name = "Engenheiro de Qualidade")]
        QualityAssuranceEngineer,
        //[Display(Name = "Project Manager")]
        [Display(Name = "Gerente de Projeto")]
        ProjectManager,
        [Display(Name = "Tester")]
        Tester,
        //[Display(Name = "Other")]
        [Display(Name = "Outro")]
        Other
    }

    public enum ScenarioOrigin
    {
        [Display(Name = "Relato")]
        Report = 1,
        [Display(Name = "Entrevista")]
        Interview,
        [Display(Name = "Documento")]
        Document
    }

    public enum Status
    {
        [Display(Name = "Percebido")]
        [Description("Percebido")]
        Perceived = 1,
        [Display(Name = "Em Evolução")]
        [Description("Em Evolução")]
        InEvolution,
        [Display(Name = "Com Intervenção")]
        [Description("Com Intervenção")]
        WithIntervention,
        [Display(Name = "Finalizado")]
        [Description("Finalizado")]
        Finished

    }

    public enum EmpathyMapType
    {
        [Display(Name = "Pensa")]
        Think = 1,
        [Display(Name = "Vê")]
        See,
        [Display(Name = "Faz")]
        Do,
        [Display(Name = "Faz")]
        Speak,
        [Display(Name = "Escuta")]
        Listen,
        [Display(Name = "Sente")]
        Feel,
        [Display(Name = "Dores")]
        Pain,
        [Display(Name = "Necessidades")]
        Need
        //[Display(Name = "Think")]
        //Think = 1,
        //[Display(Name = "See")]
        //See,
        //[Display(Name = "Do")]
        //Do,
        //[Display(Name = "Speak")]
        //Speak,
        //[Display(Name = "Listen")]
        //Listen,
        //[Display(Name = "Feel")]
        //Feel,
        //[Display(Name = "Pain")]
        //Pain,
        //[Display(Name = "Need")]
        //Need

    }

    public enum PublicOrPrivate
    {
        Public = 1,
        Private
    }

    public enum Emotions
    {
        [Display(Name = "Selecione uma opção")]
        NotInformed = 0,
        [Display(Name = "Alegria")]
        Happyness,
        [Display(Name = "Tristeza")]
        Sadness,
        [Display(Name = "Raiva")]
        Angryness,
        [Display(Name = "Medo")]
        Fearness
    }

    public enum Feelings
    {
        [Display(Name = "Selecione")]
        Select = 1,

        //happy

        [Display(Name = "Emocionado")]
        Thrilled,
        [Display(Name = "Entusiasmado")]
        Enthusiastic,
        [Display(Name = "Feliz")]
        Happy,
        [Display(Name = "Animação")]
        CheeredUp,
        [Display(Name = "Alegria")]
        Joy,
        [Display(Name = "Gratidão")]
        Gratitude,
        [Display(Name = "Compreensão")]
        Understanding,
        [Display(Name = "Confiança")]
        Confidence,
        [Display(Name = "Motivação")]
        Motivation,
        [Display(Name = "Contentamento")]
        Contentment,
        [Display(Name = "Aceitação")]
        Acceptance,
        [Display(Name = "Harmonia")]
        Harmony,
        [Display(Name = "Tranquilidade")]
        Tranquility,
        [Display(Name = "Serenidade")]
        Serenity,

        //sad

        [Display(Name = "Desalento")]
        Dismay,
        [Display(Name = "Aflição")]
        Affliction,
        [Display(Name = "Preocupação")]
        Worry,
        [Display(Name = "Deprimido")]
        Depressed,
        [Display(Name = "Mágoa")]
        Hurt,
        [Display(Name = "Desânimo")]
        Discouragement,
        [Display(Name = "Abatido")]
        Dejected,
        [Display(Name = "Desespero")]
        Despair,
        [Display(Name = "Fracasso")]
        Failure,
        [Display(Name = "Desapontado")]
        Disappointed,
        [Display(Name = "Angústia")]
        Anguish,
        [Display(Name = "Melancolia")]
        Melancholy,
        [Display(Name = "Infeliz")]
        Unhappy,
        [Display(Name = "Intranquilo")]
        Uneasy,

        //angry

        [Display(Name = "Irritação")]
        Irritation,
        [Display(Name = "Frustração")]
        Frustration,
        [Display(Name = "Zangado")]
        Angry,
        [Display(Name = "Mau humor")]
        BadMood,
        [Display(Name = "Fúria")]
        Fury,
        [Display(Name = "Indignação")]
        Indignation,
        [Display(Name = "Aborrecimento")]
        Annoyance,
        [Display(Name = "Animosidade")]
        Animosity,
        [Display(Name = "Malícia")]
        Malice,
        [Display(Name = "Ódio")]
        Hate,
        [Display(Name = "Má vontade")]
        IllWill,
        [Display(Name = "Hostilidade")]
        Hostility,
        [Display(Name = "Ressentimento")]
        Resentment,
        [Display(Name = "Rancor")]
        Bitterness,

        //fear

        [Display(Name = "Apreensão")]
        Seizure,
        [Display(Name = "Pânico")]
        Scared,
        [Display(Name = "Nervosismo")]
        Nervousness,
        [Display(Name = "Incerteza")]
        Uncertainty,
        [Display(Name = "Receio")]
        Fear,
        [Display(Name = "Pavor")]
        Dread,
        [Display(Name = "Ansioso")]
        Anxious,
        [Display(Name = "Desconfiança")]
        Distrust,
        [Display(Name = "Assustado")]
        Creeped,
        [Display(Name = "Surpreendido")]
        Surprised,
        [Display(Name = "Horrorizado")]
        Horrified,
        [Display(Name = "Vulnerável")]
        Vulnerable,
        [Display(Name = "Hesitante")]
        Hesitant,
        [Display(Name = "Indefeso")]
        Helpless
    }

    public enum HappyFeelings
    {
        [Display(Name = "Emocionado")]
        Thrilled = 1,
        [Display(Name = "Entusiasmado")]
        Enthusiastic,
        [Display(Name = "Feliz")]
        Happy,
        [Display(Name = "Animação")]
        CheeredUp,
        [Display(Name = "Alegria")]
        Joy,
        [Display(Name = "Gratidão")]
        Gratitude,
        [Display(Name = "Compreensão")]
        Understanding,
        [Display(Name = "Confiança")]
        Confidence,
        [Display(Name = "Motivação")]
        Motivation,
        [Display(Name = "Contentamento")]
        Contentment,
        [Display(Name = "Aceitação")]
        Acceptance,
        [Display(Name = "Harmonia")]
        Harmony,
        [Display(Name = "Tranquilidade")]
        Tranquility,
        [Display(Name = "Serenidade")]
        Serenity
    }

    public enum SadFeelings
    {
        [Display(Name = "Desalento")]
        Dismay = 1,
        [Display(Name = "Aflição")]
        Affliction,
        [Display(Name = "Preocupação")]
        Worry,
        [Display(Name = "Deprimido")]
        Depressed,
        [Display(Name = "Mágoa")]
        Hurt,
        [Display(Name = "Desânimo")]
        Discouragement,
        [Display(Name = "Abatido")]
        Dejected,
        [Display(Name = "Desespero")]
        Despair,
        [Display(Name = "Fracasso")]
        Failure,
        [Display(Name = "Desapontado")]
        Disappointed,
        [Display(Name = "Angústia")]
        Anguish,
        [Display(Name = "Melancolia")]
        Melancholy,
        [Display(Name = "Infeliz")]
        Unhappy,
        [Display(Name = "Intranquilo")]
        Uneasy,
    }

    public enum AngryFeelings
    {
        [Display(Name = "Irritação")]
        Irritation = 1,
        [Display(Name = "Frustração")]
        Frustration,
        [Display(Name = "Zangado")]
        Angry,
        [Display(Name = "Mau humor")]
        BadMood,
        [Display(Name = "Fúria")]
        Fury,
        [Display(Name = "Indignação")]
        Indignation,
        [Display(Name = "Aborrecimento")]
        Annoyance,
        [Display(Name = "Animosidade")]
        Animosity,
        [Display(Name = "Malícia")]
        Malice,
        [Display(Name = "Ódio")]
        Hate,
        [Display(Name = "Má vontade")]
        IllWill,
        [Display(Name = "Hostilidade")]
        Hostility,
        [Display(Name = "Ressentimento")]
        Resentment,
        [Display(Name = "Rancor")]
        Bitterness
    }

    public enum FearFeelings
    {
        [Display(Name = "Apreensão")]
        Seizure = 1,
        [Display(Name = "Pânico")]
        Scared,
        [Display(Name = "Nervosismo")]
        Nervousness,
        [Display(Name = "Incerteza")]
        Uncertainty,
        [Display(Name = "Receio")]
        Fear,
        [Display(Name = "Pavor")]
        Dread,
        [Display(Name = "Ansioso")]
        Anxious,
        [Display(Name = "Desconfiança")]
        Distrust,
        [Display(Name = "Assustado")]
        Creeped,
        [Display(Name = "Surpreendido")]
        Surprised,
        [Display(Name = "Horrorizado")]
        Horrified,
        [Display(Name = "Vulnerável")]
        Vulnerable,
        [Display(Name = "Hesitante")]
        Hesitant,
        [Display(Name = "Indefeso")]
        Helpless,
    }

    public enum Behavior
    {
        [Display(Name = "Selecione")]
        Select = 1,
        [Display(Name = "Ataca")]
        Attack,
        [Display(Name = "Evita")]
        Avoid,
        [Display(Name = "Informa")]
        Informs,
        [Display(Name = "Está Aberto")]
        IsOpen,
        [Display(Name = "Une")]
        Unite,
    }

    public enum StakeholderPersonality
    {
        [Display(Name = "Normal")]
        Normal = 1,
        [Display(Name = "Sabe tudo")]
        KnowsAll,
        [Display(Name = "Talvez")]
        Maybe,
        [Display(Name = "Chorão")]
        Cry,
        [Display(Name = "Calado")]
        Quiet,
        [Display(Name = "Granada")]
        Grenade
    }

    public enum Origin
    {
        [Display(Name = "Econômica")]
        Economic = 1,
        [Display(Name = "Valor")]
        Value,
        [Display(Name = "Poder")]
        Power,
        [Display(Name = "Outro")]
        Other
    }

    public enum Level
    {
        [Display(Name = "Individual")]
        Individual = 1,
        [Display(Name = "Entre Grupos")]
        BetweenGroups,
        [Display(Name = "Entre Empresas")]
        BetweenCompanies,
        [Display(Name = "Entre Culturas")]
        BetweenCultures,
        [Display(Name = "Outro")]
        Other
    }

    public enum DevelopingType
    {

        [Display(Name = "Ágil")]
        Agile = 1,
        [Display(Name = "Waterfall")]
        Waterfall,
        [Display(Name = "Scrum")]
        Scrum,
        [Display(Name = "Kanban")]
        Kanban
    }

    public enum ConflictImpact
    {
        [Display(Name = "Construtivo")]
        Constructive = 1,
        [Display(Name = "Destrutivo")]
        Destructive,
        [Display(Name = "Neutro")]
        Neutral
    }

    public enum TeamDistribution
    {
        [Display(Name = "Distribuída")]
        Distributed = 1
    }
}

﻿using Domain.Entities.Relationships;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class EffectConsequence : BaseEntity
    {
        public string EffectAndConsequence { get; set; }
        public ICollection<ScenarioEffectConsequence> ScenarioEffectConsequence { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }

        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

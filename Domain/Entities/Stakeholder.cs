﻿using Domain.Entities.Relationships;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Stakeholder : BaseEntity
    {
        public StakeholderPersonality Description { get; set; }

        public StakeholderType Type { get; set; }


        public ICollection<StakeholderEmpathyMap> StakeholderEmpathyMap { get; set; }


        public Scenario Scenario { get; set; }
        public int ScenarioId { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }


        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class ConflictJourney : BaseEntity
    {
        public Feelings FeelingStakeholder1 { get; set; }
        public Behavior BehaviorStakeholder1 { get; set; }

        public string Stage { get; set; }

        public Feelings FeelingStakeholder2 { get; set; }
        public Behavior BehaviorStakeholder2 { get; set; }

        public Scenario Scenario { get; set; }
        public int ScenarioId { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }

        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Company : BaseEntity
    {
        public string Description { get; set; }
        public PublicOrPrivate PublicOrPrivate { get; set; }
        public string Type { get; set; }
        public int EmployeesNumber { get; set; }
        public ICollection<Scenario> Scenarios { get; set; }


        public int? UserId { get; set; }
        public User User { get; set; }

        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using Domain.Entities.Relationships;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class SEPhase : BaseEntity
    {
        public string Phase { get; set; }
        public ICollection<ScenarioSEPhase> ScenarioSEPhase { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }


        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

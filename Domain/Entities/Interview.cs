﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class Interview : BaseEntity
    {
        public string Title { get; set; }
        public string Interviewer { get; set; }
        public DateTime Realization { get; set; }

        public ICollection<Scenario> Scenarios { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }

        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

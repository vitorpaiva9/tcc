﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Severity : BaseEntity
    {
        public string Type { get; set; }

        public int Level { get; set; }

        public ICollection<Scenario> Scenarios { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }

        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

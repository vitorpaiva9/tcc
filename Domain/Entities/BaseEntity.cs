﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }

        public IDictionary<string, string> Errors;

        public bool IsValid()
        {
            Errors = new Dictionary<string, string>();
            Validate();
            return !(Errors.Any());
        }

        public abstract void Validate();
    }
}

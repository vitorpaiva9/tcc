﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Relationships
{
    public class ScenarioManageConflict : BaseEntity
    {
        public int ScenarioId { get; set; }
        public Scenario Scenario { get; set; }
        public int ManageConflictId { get; set; }
        public ManageConflict ManageConflict { get; set; }

        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

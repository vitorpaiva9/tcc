﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Relationships
{
    public class StakeholderEmpathyMap : BaseEntity
    {
        public int StakeholderId { get; set; }
        public Stakeholder Stakeholder { get; set; }

        public int EmpathyMapId { get; set; }
        public EmpathyMap EmpathyMap { get; set; }

        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

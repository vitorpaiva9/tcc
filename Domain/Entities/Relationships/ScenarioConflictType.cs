﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Relationships
{
    public class ScenarioConflictType : BaseEntity
    {
        public int ScenarioId { get; set; }
        public Scenario Scenario { get; set; }
        public int ConflictTypeId { get; set; }
        public ConflictType ConflictType { get; set; }


        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Relationships
{
    public class ScenarioSEPhase : BaseEntity
    {
        public int ScenarioId { get; set; }
        public Scenario Scenario { get; set; }
        public int SEPhaseId { get; set; }
        public SEPhase SEPhase { get; set; }

        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Relationships
{
    public class ScenarioEffectConsequence : BaseEntity
    {
        public int ScenarioId { get; set; }
        public Scenario Scenario { get; set; }
        public int EffectConsequenceId { get; set; }
        public EffectConsequence EffectConsequence { get; set; }
        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using Domain.Entities.Relationships;
using Domain.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Scenario : BaseEntity
    {
        public string Summary { get; set; }

        public string TranscriptionPath { get; set; }

        public string AudioPath { get; set; }


        public string ProblemSummary { get; set; }

        public ScenarioOrigin? ScenarioOrigin { get; set; }

        public Status? Status { get; set; }
        public ConflictImpact? ConflictImpact { get; set; }
        public Level? Level { get; set; }
        public Origin? Origin { get; set; }

        public DevelopingType? DevelopingType { get; set; }

        public string TeamLocalization { get; set; }





        public ICollection<ScenarioConflictType> ScenarioConflictType { get; set; }
        public ICollection<ScenarioSEPhase> ScenarioSEPhase { get; set; }
        public ICollection<ScenarioManageConflict> ScenarioManageConflict { get; set; }
        public ICollection<ScenarioEffectConsequence> ScenarioEffectConsequence { get; set; }
        public ICollection<Stakeholder> Stakeholders { get; set; }
        public ICollection<ConflictJourney> Evolutions { get; set; }





        public int? SeverityId { get; set; }
        public Severity Severity { get; set; }

        public int? CompanyId { get; set; }
        public Company Company { get; set; }

        public int? InterviewId { get; set; }
        public Interview Interview { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }

        public int? StakeholderRelatorId { get; set; }
        public Stakeholder StakeholderRelator { get; set; }


        public int? ContextId { get; set; }
        public ConflictJourney Context { get; set; }
        public int? PerceptionId { get; set; }

        public ConflictJourney Perception { get; set; }
        public int? ClimaxId { get; set; }

        public ConflictJourney Climax { get; set; }
        public int? AdministrationId { get; set; }

        public ConflictJourney Administration { get; set; }
        public int? ConsequenceId { get; set; }

        public ConflictJourney Consequence { get; set; }

        public string DescriptionStakeholder1 { get; set; }

        public string DescriptionStakeholder2 { get; set; }


        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

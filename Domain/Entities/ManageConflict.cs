﻿using Domain.Entities.Relationships;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class ManageConflict : BaseEntity
    {
        public string Style { get; set; }
        public string Description { get; set; }
        public string Effect { get; set; }
        public ICollection<ScenarioManageConflict> ScenarioManageConflict { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }

        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

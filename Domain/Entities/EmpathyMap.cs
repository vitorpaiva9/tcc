﻿using Domain.Entities.Relationships;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class EmpathyMap : BaseEntity
    {
        public EmpathyMapType EmpathyMapType { get; set; }

        public string Description { get; set; }

        public ICollection<StakeholderEmpathyMap> StakeholderEmpathyMap { get; set; }

        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

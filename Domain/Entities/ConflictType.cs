﻿using Domain.Entities.Relationships;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class ConflictType : BaseEntity
    {
        public string Type { get; set; }
        public ICollection<ScenarioConflictType> ScenarioConflictType { get; set; }


        public int? UserId { get; set; }
        public User User { get; set; }

        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

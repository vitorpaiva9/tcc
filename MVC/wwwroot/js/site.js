﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.




// Toggle the side navigation
$("#sidebarToggle").on('click', function (e) {
    e.preventDefault();
    
    if ($('.sidebar').is(':visible')) {
        $('.sidebar').hide();
    } else {
        $('.sidebar').show();
    }
});

// Prevent the content wrapper from scrolling when the fixed side navigation hovered over
$('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
    if ($(window).width() > 768) {
        var e0 = e.originalEvent,
            delta = e0.wheelDelta || -e0.detail;
        this.scrollTop += (delta < 0 ? 1 : -1) * 30;
        e.preventDefault();
    }
});

// Scroll to top button appear
$(document).on('scroll', function () {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
        $('.scroll-to-top').fadeIn();
    } else {
        $('.scroll-to-top').fadeOut();
    }
});

// Smooth scrolling using jQuery easing
$(document).on('click', 'a.scroll-to-top', function (event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
        scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    event.preventDefault();
});

$(document).on('click', '.dropdown-menu', function (event) {
    event.stopPropagation();
});

function initializeWizard() {
    $('#smartwizard').smartWizard({
        selected: 0,  // Initial selected step, 0 = first step
        keyNavigation: false, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
        autoAdjustHeight: true, // Automatically adjust content height
        lang: {  // Language variables
            //next: 'Next',
            //previous: 'Previous'
            next: 'Próximo',
            previous: 'Anterior'
        },
        toolbarSettings: {
            toolbarPosition: 'bottom', // none, top, bottom, both
            toolbarButtonPosition: 'center', // left, right
            showNextButton: true, // show/hide a Next button
            showPreviousButton: true, // show/hide a Previous button
            toolbarExtraButtons: [
                //$('<button></button>').text('Save and Finish')
                $('<button></button>').text('Salvar e Finalizar')
                    .addClass('btn btn-info finish-scenario')
                    .on('click', function () {

                        $('#text-message-modal').html('As informações do Cenário preenchidas até agora serão salvas, você poderá finalizar o preenchimento em outro momento');
                        $('#message-modal').modal('show');

                    })
            ]

        },
        anchorSettings: {
            anchorClickable: true, // Enable/Disable anchor navigation
            enableAllAnchors: true, // Activates all anchors clickable all times
            markDoneStep: false, // add done css
            enableAnchorOnDoneStep: false // Enable/Disable the done steps navigation

        }
    });


    $(document).on('click', '#ok-message-modal', function () {
        saveAndReturnWizard();
    });
}

function saveAndReturnWizard() {
    if ($('#smartwizard').data('smartWizard').current_index === 0) {

        $('#save-return').val(true);
        $('#smartwizard').smartWizard("next");
    } else {
        $('#save-return').val(true);
        $("#smartwizard").trigger("leaveStep", ["", $('#smartwizard').data('smartWizard').current_index, "forward"]);
        //$('#smartwizard').smartWizard("next");
    }
}


﻿using System;
using System.Linq;
using System.Security.Claims;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces;
using Services.ViewModels;

namespace MVC.Controllers
{
    public class ManageConflictController : Controller
    {
        private readonly IManageConflictService _manageConflictService;


        public ManageConflictController(IManageConflictService manageConflict)
        {
            _manageConflictService = manageConflict;
        }

        public IActionResult Index()
        {
            //ViewBag.Title = "Manage Conflict Form";
            ViewBag.Title = "Administração de Conflito";

            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;


            var indexViewModel = new ManageConflictIndexViewModel()
            {
                ManageConflictList = _manageConflictService.GetAll().ToList(),
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                IsAdmin = isAdmin
            };
            return View(indexViewModel);

        }

        public IActionResult Create()
        {
            //ViewBag.Title = "Manage Conflict Form - Create";
            ViewBag.Title = "Administração de Conflito / Criar";
            return View(new ManageConflictViewModel());
        }

        public IActionResult CreateModal()
        {
            //ViewBag.Title = "Manage Conflict Form - Create";
            ViewBag.Title = "Administração de Conflito / Criar";
            return PartialView(nameof(Create), new ManageConflictViewModel() { FromModal = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ManageConflictViewModel manageConflictViewModel)
        {
            if (ModelState.IsValid)
            {
                manageConflictViewModel.UserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value);

                _manageConflictService.Add(manageConflictViewModel);

                if (manageConflictViewModel.FromModal)
                {
                    var scenarioConflictViewModel = new ScenarioConflictViewModel();
                    scenarioConflictViewModel.ManageConflictList = _manageConflictService.GetAll();
                    return PartialView("DropdownManageConflict", scenarioConflictViewModel);
                }
                else
                {
                    return RedirectToAction(nameof(Index));
                }
            }

            if (manageConflictViewModel.FromModal)
            {
                Response.StatusCode = 400;
                return PartialView(nameof(Create), manageConflictViewModel);
            }
            else
            {
                return View(manageConflictViewModel);
            }
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var manageConflictViewModel = _manageConflictService.Get(id);
            if (manageConflictViewModel == null)
            {
                return NotFound();
            }
            return PartialView(manageConflictViewModel);
        }


        [HttpGet]
        public IActionResult Edit(int id)
        {

            ViewBag.Title = "Administração de Conflito / Editar";

            if (id == 0)
            {
                return NotFound();
            }

            var viewModel = _manageConflictService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            var manageConflictViewModel = _manageConflictService.Get(id);
            if (manageConflictViewModel == null)
            {
                return NotFound();
            }
            return View(manageConflictViewModel);
        }

        [HttpPost]
        public IActionResult Edit(int id, ManageConflictViewModel manageConflictViewModel)
        {
            if (id != manageConflictViewModel.Id)
            {
                return NotFound();
            }

            var viewModel = _manageConflictService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _manageConflictService.Update(manageConflictViewModel);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_manageConflictService.GetAll().Any(e => e.Id == id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(manageConflictViewModel);
        }

        [HttpGet]
        public IActionResult TableList()
        {
            return PartialView(_manageConflictService.GetAll());
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            var viewModel = _manageConflictService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (_manageConflictService.CanRemove(id))
            {
                _manageConflictService.Remove(id);
            }
            else
            {
                //return StatusCode(500, "This item is being used by some Scenario and can not be removed");
                return StatusCode(500, "Este item está sendo usado por algum Cenário e não pode ser removido");
            }




            var indexViewModel = new ManageConflictIndexViewModel()
            {
                ManageConflictList = _manageConflictService.GetAll().ToList(),
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                IsAdmin = isAdmin
            };

            return PartialView(nameof(TableList), indexViewModel);
        }
    }
}
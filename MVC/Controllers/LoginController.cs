﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Policy;
using System.Security.Principal;
using System.Threading.Tasks;
using Domain.Entities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.ViewModels;

namespace MVC.Controllers
{
    public class LoginController : Controller
    {
        private IUserService _userService { get; set; }
        public LoginController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Login()
        {
            return View(new LoginViewModel());
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel login)
        {

            var usuario = _userService.Login(login);

            if (usuario != null)
            {
                var claims = new List<Claim>()
                {
                        new Claim(ClaimTypes.NameIdentifier, usuario.Username.ToString()),
                        new Claim(ClaimTypes.Role, usuario.Role.ToString()),
                        new Claim(ClaimTypes.Hash, usuario.Id.ToString()),
                };

                var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var principal = new ClaimsPrincipal(identity);
                var authProperties = new AuthenticationProperties

                {
                    AllowRefresh = true,
                    IsPersistent = true,
                    ExpiresUtc = DateTime.UtcNow.AddHours(2)
                };

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, authProperties);
                


                return RedirectToAction("Index", "Home");
            }
            else
            {
                //ModelState.AddModelError(nameof(LoginViewModel.Password), "Password Invalid");
                ModelState.AddModelError(nameof(LoginViewModel.Password), "Senha Inválida");
                return View();

            }
        }


        [AllowAnonymous]
        [HttpGet]
        public IActionResult SignIn()
        {
            return View(new UserViewModel());
        }


        [AllowAnonymous]
        [HttpPost]
        public IActionResult SignIn(UserViewModel login)
        {
            if (ModelState.IsValid)
            {
                var user = _userService.Add(login);

                if (user != null)
                {
                    return RedirectToAction(nameof(Login));
                }

                //ModelState.AddModelError(nameof(UserViewModel.Username), "Username already exists");
                ModelState.AddModelError(nameof(UserViewModel.Username), "Usuário já existente");
            }

            return View(login);


        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction(nameof(Login));
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult UnauthorizedAcess()
        {
            return View();
        }

    }
}
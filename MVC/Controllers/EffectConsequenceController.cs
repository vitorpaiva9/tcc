﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces;
using Services.ViewModels;

namespace MVC.Controllers
{
    public class EffectConsequenceController : Controller
    {
        private readonly IEffectConsequenceService _effectConsequenceService;


        public EffectConsequenceController(IEffectConsequenceService effectConsequenceService)
        {
            _effectConsequenceService = effectConsequenceService;
        }

        public IActionResult Index()
        {
            //ViewBag.Title = "Effect Consequence";
            ViewBag.Title = "Efeitos e Consequências";

            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;


            var indexViewModel = new EffectConsequenceIndexViewModel()
            {
                EffectConsequenceList = _effectConsequenceService.GetAll().ToList(),
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                IsAdmin = isAdmin
            };
            return View(indexViewModel);

        }

        public IActionResult Create()
        {
            //ViewBag.Title = "Effect Consequence - Create";
            ViewBag.Title = "Efeitos e Consequências / Criar";
            return View(new EffectConsequenceViewModel());
        }

        public IActionResult CreateModal()
        {
            //ViewBag.Title = "Effect Consequence - Create";
            ViewBag.Title = "Efeitos e Consequências / Criar";
            return PartialView(nameof(Create), new EffectConsequenceViewModel() { FromModal = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(EffectConsequenceViewModel effectConsequenceViewModel)
        {
            if (ModelState.IsValid)
            {
                effectConsequenceViewModel.UserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value);

                _effectConsequenceService.Add(effectConsequenceViewModel);

                if (effectConsequenceViewModel.FromModal)
                {
                    var scenarioConflictJourneyViewModel = new ScenarioConflictJourneyViewModel();
                    scenarioConflictJourneyViewModel.EffectConsequenceList = _effectConsequenceService.GetAll();
                    return PartialView("DropdownEffectConsequence", scenarioConflictJourneyViewModel);
                }
                else
                {
                    return RedirectToAction(nameof(Index));
                }
            }

            if (effectConsequenceViewModel.FromModal)
            {
                Response.StatusCode = 400;
                return PartialView(nameof(Create), effectConsequenceViewModel);
            }
            else
            {
                return View(effectConsequenceViewModel);
            }
        }
        [HttpGet]
        public IActionResult Details(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var effectConsequenceViewModel = _effectConsequenceService.Get(id);
            if (effectConsequenceViewModel == null)
            {
                return NotFound();
            }
            return PartialView(effectConsequenceViewModel);
        }


        [HttpGet]
        public IActionResult Edit(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var viewModel = _effectConsequenceService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            var effectConsequenceViewModel = _effectConsequenceService.Get(id);
            if (effectConsequenceViewModel == null)
            {
                return NotFound();
            }
            return View(effectConsequenceViewModel);
        }

        [HttpPost]
        public IActionResult Edit(int id, EffectConsequenceViewModel effectConsequenceViewModel)
        {
            ViewBag.Title = "Efeitos e Consequências / Editar";

            if (id != effectConsequenceViewModel.Id)
            {
                return NotFound();
            }

            var viewModel = _effectConsequenceService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _effectConsequenceService.Update(effectConsequenceViewModel);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_effectConsequenceService.GetAll().Any(e => e.Id == id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(effectConsequenceViewModel);
        }

        [HttpGet]
        public IActionResult TableList()
        {
            return PartialView(_effectConsequenceService.GetAll());
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            var viewModel = _effectConsequenceService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (_effectConsequenceService.CanRemove(id))
            {
                _effectConsequenceService.Remove(id);
            }
            else
            {
                //return StatusCode(500, "This item is being used by some Scenario and can not be removed");
                return StatusCode(500, "Este item está sendo usado por algum Cenário e não pode ser removido");

            }



            var indexViewModel = new EffectConsequenceIndexViewModel()
            {
                EffectConsequenceList = _effectConsequenceService.GetAll().ToList(),
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                IsAdmin = isAdmin
            };

            return PartialView(nameof(TableList), indexViewModel);
        }
    }
}
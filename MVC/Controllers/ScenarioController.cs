﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Domain.Entities;
using System.Collections.Generic;
using Services.ViewModels;
using Services.Interfaces;
using System.Security.Claims;
using System;
using Domain.Enums;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using System.IO;
using MimeKit;

namespace MVC.Controllers
{
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params Roles[] allowedRoles)
        {
            var allowedRolesAsStrings = allowedRoles.Select(x => (int)x);
            Roles = string.Join(",", allowedRolesAsStrings);
        }
    }

    public class ScenarioController : Controller
    {
        private readonly IScenarioService _scenarioService;
        private readonly IConflictTypeService _conflictTypeService;
        private readonly ISeverityService _severityService;
        private readonly ISEPhaseService _sePhaseService;
        private readonly IManageConflictService _manageConflictService;
        private readonly IEffectConsequenceService _effectConsequenceService;
        private readonly ICompanyService _companyService;
        private readonly IInterviewService _interviewService;
        public ScenarioController(IScenarioService scenarioService, IConflictTypeService conflictTypeService,
            ISeverityService severityService, ISEPhaseService sePhaseService, IManageConflictService manageConflictService,
            IEffectConsequenceService effectConsequenceService, ICompanyService companyService, IInterviewService interviewService)
        {
            _scenarioService = scenarioService;
            _conflictTypeService = conflictTypeService;
            _severityService = severityService;
            _sePhaseService = sePhaseService;
            _manageConflictService = manageConflictService;
            _effectConsequenceService = effectConsequenceService;
            _companyService = companyService;
            _interviewService = interviewService;
        }

        public IActionResult Index()
        {
            //ViewBag.Title = "Scenarios";
            ViewBag.Title = "Cenário";

            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var indexViewModel = new ScenarioIndexViewModel()
            {
                ScenarioViewModelList = _scenarioService.GetAll(),
                IsAdmin = isAdmin,
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                ConflictTypeList = _conflictTypeService.GetAll(),
                ManageConflictList = _manageConflictService.GetAll(),
                SEPhaseList = _sePhaseService.GetAll(),
                SeverityList = _severityService.GetAll()
            };
            return View(indexViewModel);
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Title = "Cenário / Criar";

            var scenarioViewModel = new ScenarioViewModel();

            scenarioViewModel.ScenarioConflictViewModel.ConflictTypeList = _conflictTypeService.GetAll();
            scenarioViewModel.ScenarioConflictViewModel.SeverityList = _severityService.GetAll();
            scenarioViewModel.ScenarioConflictViewModel.SEPhaseList = _sePhaseService.GetAll();
            scenarioViewModel.ScenarioConflictViewModel.ManageConflictList = _manageConflictService.GetAll();
            scenarioViewModel.ScenarioConflictJourneyViewModel.EffectConsequenceList = _effectConsequenceService.GetAll();
            scenarioViewModel.ScenarioGeneralViewModel.CompanyList = _companyService.GetAll();
            scenarioViewModel.ScenarioGeneralViewModel.InterviewList = _interviewService.GetAll();

            return View(scenarioViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ScenarioViewModel scenarioViewModel)
        {

            if (scenarioViewModel.ScenarioGeneralViewModel.CompanyId == 0)
            {
                scenarioViewModel.ScenarioGeneralViewModel.CompanyId = null;
            }

            if (scenarioViewModel.ScenarioGeneralViewModel.ScenarioOrigin.Equals(ScenarioOrigin.Interview) && scenarioViewModel.ScenarioGeneralViewModel.InterviewId == 0)
            {
                //ModelState.AddModelError(nameof(scenarioViewModel.ScenarioGeneralViewModel.InterviewId), "The Interview field is required.");
                ModelState.AddModelError(nameof(scenarioViewModel.ScenarioGeneralViewModel.InterviewId), "A Entrevista é obrigatória.");
            }

            if (ModelState.IsValid)
            {
                scenarioViewModel.ScenarioGeneralViewModel.UserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value);
                _scenarioService.Add(scenarioViewModel);


                return RedirectToAction(nameof(Index));
            }

            scenarioViewModel.ScenarioConflictViewModel.ConflictTypeList = _conflictTypeService.GetAll();
            scenarioViewModel.ScenarioConflictViewModel.SeverityList = _severityService.GetAll();
            scenarioViewModel.ScenarioConflictViewModel.SEPhaseList = _sePhaseService.GetAll();
            scenarioViewModel.ScenarioConflictViewModel.ManageConflictList = _manageConflictService.GetAll();
            scenarioViewModel.ScenarioConflictJourneyViewModel.EffectConsequenceList = _effectConsequenceService.GetAll();
            scenarioViewModel.ScenarioGeneralViewModel.CompanyList = _companyService.GetAll();
            scenarioViewModel.ScenarioGeneralViewModel.InterviewList = _interviewService.GetAll();

            return View(scenarioViewModel);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateGeneral(ScenarioGeneralViewModel viewModel)
        {
            if (viewModel.TranscriptionFile != null)
            {

                var extension = Path.GetExtension(viewModel.TranscriptionFile.FileName);
                if (Path.GetExtension(viewModel.TranscriptionFile.FileName) != ".txt" && Path.GetExtension(viewModel.TranscriptionFile.FileName) != ".doc" && Path.GetExtension(viewModel.TranscriptionFile.FileName) != ".docx")
                {
                    ModelState.AddModelError(nameof(viewModel.TranscriptionFile), "Tipo de arquivo inválido.");

                }
            }

            if (viewModel.AudioFile != null)
            {

                var extension = Path.GetExtension(viewModel.AudioFile.FileName);
                if (Path.GetExtension(viewModel.AudioFile.FileName) != ".mp3" && Path.GetExtension(viewModel.AudioFile.FileName) != ".wma" && Path.GetExtension(viewModel.AudioFile.FileName) != ".aac")
                {
                    ModelState.AddModelError(nameof(viewModel.AudioFile), "Tipo de arquivo inválido.");

                }
            }

            if (viewModel.ScenarioOrigin.Equals(ScenarioOrigin.Interview) && viewModel.InterviewId == 0)
            {
                ModelState.Clear();
                viewModel.HasError = true;
                //ModelState.AddModelError(nameof(ViewModel.InterviewId), "The Interview field is required.");
                ModelState.AddModelError(nameof(viewModel.InterviewId), "A Entrevista é obrigatória.");
            }

            if (ModelState.IsValid)
            {
                viewModel.UserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value);

                viewModel = await _scenarioService.AddGeneral(viewModel);
                ModelState.Clear();

            }
            else
            {
                ModelState.Remove("HasError");
                viewModel.HasError = true;

            }

            viewModel.CompanyList = _companyService.GetAll();
            viewModel.InterviewList = _interviewService.GetAll();

            return PartialView("_FormControlGeneral", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateStakeholders(ScenarioStakeholdersViewModel ViewModel)
        {

            if (ModelState.IsValid)
            {

                ViewModel = _scenarioService.AddStakeholders(ViewModel);

                var index = ViewModel.Stakeholders.FindIndex(x => x.Id == ViewModel.StakeholderRelatorId);

                if (index >= 0)
                {
                    ViewModel.RelatorIndex.Add(index);
                }

                ModelState.Clear();

            }
            else
            {
                ModelState.Remove("HasErrorStakeholders");
                ViewModel.HasErrorStakeholders = true;

            }

            return PartialView("_FormControlStakeholders", ViewModel);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateConflict(ScenarioConflictViewModel ViewModel)
        {

            if (ModelState.IsValid)
            {

                ViewModel = _scenarioService.AddConflict(ViewModel);

                ModelState.Clear();

            }
            else
            {
                ModelState.Remove("HasErrorConflict");
                ViewModel.HasErrorConflict = true;

            }


            ViewModel.ConflictTypeList = _conflictTypeService.GetAll();
            ViewModel.SeverityList = _severityService.GetAll();
            ViewModel.SEPhaseList = _sePhaseService.GetAll();
            ViewModel.ManageConflictList = _manageConflictService.GetAll();

            return PartialView("_FormControlConflict", ViewModel);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateConflictJourney(ScenarioConflictJourneyViewModel ViewModel)
        {

            if (ModelState.IsValid)
            {

                ViewModel = _scenarioService.AddConflictJourney(ViewModel);

                ModelState.Clear();

            }


            ViewModel.EffectConsequenceList = _effectConsequenceService.GetAll();

            return PartialView("_FormControlConflictJourney", ViewModel);

        }

        public IActionResult Details(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var scenarioViewModel = _scenarioService.Get(id);

            if (scenarioViewModel.ScenarioStakeholdersViewModel.Stakeholders.Count == 0)
            {
                scenarioViewModel.ScenarioStakeholdersViewModel.Stakeholders.Add(new StakeholderViewModel());
            }
            else
            {
                scenarioViewModel.ScenarioStakeholdersViewModel.Stakeholders = scenarioViewModel.ScenarioStakeholdersViewModel.Stakeholders.OrderByDescending(x => x.Id).ToList();

            }

            var index = scenarioViewModel.ScenarioStakeholdersViewModel.Stakeholders.FindIndex(x => x.Id == scenarioViewModel.ScenarioStakeholdersViewModel.StakeholderRelatorId);

            if (index >= 0)
            {
                scenarioViewModel.ScenarioStakeholdersViewModel.RelatorIndex.Add(index);
            }

            scenarioViewModel.ScenarioConflictViewModel.ConflictTypeList = _conflictTypeService.GetAll().Where(x => scenarioViewModel.ScenarioConflictViewModel.ConflictTypeIds.Contains(x.Id));
            scenarioViewModel.ScenarioConflictViewModel.SeverityList = _severityService.GetAll().Where(x => scenarioViewModel.ScenarioConflictViewModel.SeverityId != null && scenarioViewModel.ScenarioConflictViewModel.SeverityId.Value == x.Id);
            scenarioViewModel.ScenarioConflictViewModel.SEPhaseList = _sePhaseService.GetAll().Where(x => scenarioViewModel.ScenarioConflictViewModel.SEPhaseIds.Contains(x.Id));
            scenarioViewModel.ScenarioConflictViewModel.ManageConflictList = _manageConflictService.GetAll().Where(x => scenarioViewModel.ScenarioConflictViewModel.ManageConflictIds.Contains(x.Id));
            scenarioViewModel.ScenarioConflictJourneyViewModel.EffectConsequenceList = _effectConsequenceService.GetAll().Where(x => scenarioViewModel.ScenarioConflictJourneyViewModel.EffectConsequenceIds.Contains(x.Id));
            scenarioViewModel.ScenarioGeneralViewModel.CompanyList = _companyService.GetAll().Where(x => scenarioViewModel.ScenarioGeneralViewModel.CompanyId == x.Id);
            scenarioViewModel.ScenarioGeneralViewModel.InterviewList = _interviewService.GetAll().Where(x => scenarioViewModel.ScenarioGeneralViewModel.InterviewId == x.Id);

            if (scenarioViewModel == null)
            {
                return NotFound();
            }
            scenarioViewModel.ScenarioGeneralViewModel.IsDetails = true;
            scenarioViewModel.ScenarioConflictViewModel.IsDetails = true;
            scenarioViewModel.ScenarioStakeholdersViewModel.IsDetails = true;
            scenarioViewModel.ScenarioConflictJourneyViewModel.IsDetails = true;

            return PartialView(scenarioViewModel);
        }


        [HttpGet]
        //[AuthorizeRoles(Roles.Admin)]
        public IActionResult Edit(int id)
        {
            ViewBag.Title = "Cenário / Editar";

            if (id == 0)
            {
                return NotFound();
            }

            var scenarioViewModel = _scenarioService.Get(id);

            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(scenarioViewModel.ScenarioGeneralViewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (scenarioViewModel == null)
            {
                return NotFound();
            }

            if (scenarioViewModel.ScenarioStakeholdersViewModel.Stakeholders.Count == 0)
            {
                scenarioViewModel.ScenarioStakeholdersViewModel.Stakeholders.Add(new StakeholderViewModel());
                scenarioViewModel.ScenarioStakeholdersViewModel.Stakeholders.Add(new StakeholderViewModel());
            }
            else
            {
                scenarioViewModel.ScenarioStakeholdersViewModel.Stakeholders = scenarioViewModel.ScenarioStakeholdersViewModel.Stakeholders.OrderByDescending(x => x.Id).ToList();
            }

            scenarioViewModel.ScenarioConflictViewModel.ConflictTypeList = _conflictTypeService.GetAll();
            scenarioViewModel.ScenarioConflictViewModel.SeverityList = _severityService.GetAll();
            scenarioViewModel.ScenarioConflictViewModel.SEPhaseList = _sePhaseService.GetAll();
            scenarioViewModel.ScenarioConflictViewModel.ManageConflictList = _manageConflictService.GetAll();

            if (scenarioViewModel.ScenarioConflictJourneyViewModel.Evolutions.Count == 0)
            {
                scenarioViewModel.ScenarioConflictJourneyViewModel = new ScenarioConflictJourneyViewModel();
            }

            scenarioViewModel.ScenarioConflictJourneyViewModel.EffectConsequenceList = _effectConsequenceService.GetAll();
            scenarioViewModel.ScenarioGeneralViewModel.CompanyList = _companyService.GetAll();
            scenarioViewModel.ScenarioGeneralViewModel.InterviewList = _interviewService.GetAll();



            return View("Create", scenarioViewModel);
        }

        [HttpPost]
        //[AuthorizeRoles(Roles.Admin)]
        public IActionResult Edit(int id, ScenarioViewModel scenarioViewModel)
        {
            if (id != scenarioViewModel.Id)
            {
                return NotFound();
            }

            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(scenarioViewModel.ScenarioGeneralViewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (scenarioViewModel.ScenarioGeneralViewModel.CompanyId == 0)
            {
                scenarioViewModel.ScenarioGeneralViewModel.CompanyId = null;
            }

            if (scenarioViewModel.ScenarioGeneralViewModel.ScenarioOrigin.Equals(ScenarioOrigin.Interview) && scenarioViewModel.ScenarioGeneralViewModel.InterviewId == 0)
            {
                //ModelState.AddModelError(nameof(scenarioViewModel.ScenarioGeneralViewModel.InterviewId), "The Interview field is required.");
                ModelState.AddModelError(nameof(scenarioViewModel.ScenarioGeneralViewModel.InterviewId), "A Entrevista é obrigatória.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _scenarioService.Update(scenarioViewModel);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_scenarioService.GetAll().Any(e => e.Id == id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(scenarioViewModel);
        }

        [HttpGet]
        public IActionResult TableList()
        {
            return PartialView(_scenarioService.GetAll());
        }

        [HttpPost]
        //[AuthorizeRoles(Roles.Admin)]
        public IActionResult Delete(int id)
        {

            var scenarioViewModel = _scenarioService.Get(id);

            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(scenarioViewModel.ScenarioGeneralViewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            _scenarioService.Remove(id);

            var indexViewModel = new ScenarioIndexViewModel()
            {
                ScenarioViewModelList = _scenarioService.GetAll(),
                IsAdmin = isAdmin,
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                ConflictTypeList = _conflictTypeService.GetAll(),
                ManageConflictList = _manageConflictService.GetAll(),
                SEPhaseList = _sePhaseService.GetAll(),
                SeverityList = _severityService.GetAll()
            };

            return PartialView(nameof(TableList), indexViewModel);
        }

        [HttpPost]
        public IActionResult AddStakeholder(ScenarioStakeholdersViewModel scenarioViewModel)
        {
            scenarioViewModel.Stakeholders.Add(new StakeholderViewModel());
            ModelState.Clear();

            return PartialView("_Stakeholders", scenarioViewModel);
        }

        [HttpPost]
        public IActionResult RemoveStakeholder(ScenarioStakeholdersViewModel scenarioViewModel, int ind)
        {
            scenarioViewModel.Stakeholders.RemoveAt(ind);
            ModelState.Clear();

            return PartialView("_Stakeholders", scenarioViewModel);
        }

        [HttpPost]
        public IActionResult AddEvolution(ScenarioConflictJourneyViewModel viewModel)
        {
            viewModel.Evolutions.Add(new ConflictJourneyViewModel());
            ModelState.Clear();

            return PartialView("_Evolution", viewModel);
        }

        [HttpPost]
        public IActionResult RemoveEvolution(ScenarioConflictJourneyViewModel viewModel, int ind)
        {
            viewModel.Evolutions.RemoveAt(ind);
            ModelState.Clear();

            return PartialView("_Evolution", viewModel);
        }

        [HttpPost]
        public IActionResult ChangeEmotion(ScenarioStakeholdersViewModel scenarioViewModel, int ind)
        {

            ModelState.Clear();

            return PartialView("_Feelings", scenarioViewModel);
        }

        [HttpGet]
        public IActionResult DownloadFile(int id)
        {
            var scenarioViewModel = _scenarioService.Get(id);
            var filePath = scenarioViewModel.ScenarioGeneralViewModel.TranscriptionPath;

            if (filePath == null) return NotFound();

            return PhysicalFile(filePath, MimeTypes.GetMimeType(filePath), Path.GetFileName(filePath));

        }


        [HttpGet]
        public IActionResult DownloadAudioFile(int id)
        {
            var scenarioViewModel = _scenarioService.Get(id);
            var filePath = scenarioViewModel.ScenarioGeneralViewModel.AudioPath;

            if (filePath == null) return NotFound();

            return PhysicalFile(filePath, MimeTypes.GetMimeType(filePath), Path.GetFileName(filePath));

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Search(ScenarioIndexViewModel ViewModel)
        {

            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var list = _scenarioService.GetAll();

            if (ViewModel.Status is object)
            {
                list = list.Where(x => x.ScenarioGeneralViewModel.Status.Equals(ViewModel.Status));
            }

            if (ViewModel.SeverityId != 0)
            {
                list = list.Where(x => x.ScenarioConflictViewModel.SeverityId.Equals(ViewModel.SeverityId));
            }

            if (ViewModel.ConflictImpact is object)
            {
                list = list.Where(x => x.ScenarioConflictViewModel.ConflictImpact.Equals(ViewModel.ConflictImpact));
            }

            var newList = list.ToList();

            if (ViewModel.ManageConflictIds is object)
            {
                foreach (var item in list)
                {
                    foreach (var conflictId in ViewModel.ManageConflictIds)
                    {
                        if (!item.ScenarioConflictViewModel.ManageConflictIds.Contains(conflictId))
                        {
                            newList.Remove(item);
                        }
                        
                    }
                }
            }

            list  = newList.ToList();

            if (ViewModel.SEPhaseIds is object)
            {
                foreach (var item in list)
                {
                    foreach (var sePhaseId in ViewModel.SEPhaseIds)
                    {
                        if (!item.ScenarioConflictViewModel.SEPhaseIds.Contains(sePhaseId))
                        {
                            newList.Remove(item);
                        }

                    }
                }
            }



            var indexViewModel = new ScenarioIndexViewModel()
            {
                ScenarioViewModelList = newList,
                IsAdmin = isAdmin,
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                ConflictTypeList = _conflictTypeService.GetAll(),
                ManageConflictList = _manageConflictService.GetAll(),
                SEPhaseList = _sePhaseService.GetAll(),
                SeverityList = _severityService.GetAll()
            };

            return PartialView(nameof(TableList), indexViewModel);

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces;
using Services.ViewModels;

namespace MVC.Controllers
{
    public class InterviewController : Controller
    {
        private readonly IInterviewService _interviewService;


        public InterviewController(IInterviewService interviewService)
        {
            _interviewService = interviewService;
        }

        public IActionResult Index()
        {
            //ViewBag.Title = "Interview";
            ViewBag.Title = "Entrevista";

            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;


            var indexViewModel = new InterviewIndexViewModel()
            {
                InterviewList = _interviewService.GetAll().ToList(),
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                IsAdmin = isAdmin
            };
            return View(indexViewModel);

        }

        public IActionResult Create()
        {
            //ViewBag.Title = "Interview - Create";
            ViewBag.Title = "Entrevista / Criar";
            return View(new InterviewViewModel() { Realization = DateTime.Now });
        }

        public IActionResult CreateModal()
        {
            //ViewBag.Title = "Interview - Create";
            ViewBag.Title = "Entrevista / Criar";
            return PartialView(nameof(Create), new InterviewViewModel() { FromModal = true, Realization = DateTime.Now });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(InterviewViewModel interviewViewModel)
        {

            if (ModelState.IsValid)
            {
                interviewViewModel.UserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value);

                _interviewService.Add(interviewViewModel);

                if (interviewViewModel.FromModal)
                {
                    var scenarioGeneralViewModel = new ScenarioGeneralViewModel();
                    scenarioGeneralViewModel.InterviewList = _interviewService.GetAll();
                    return PartialView("DropdownInterview", scenarioGeneralViewModel );
                }
                else
                {
                    return RedirectToAction(nameof(Index));
                }
            }

            if (interviewViewModel.FromModal)
            {
                Response.StatusCode = 400;
                return PartialView(nameof(Create), interviewViewModel);
            }
            else
            {
                return View(interviewViewModel);
            }
        }

        public IActionResult Details(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var interviewViewModel = _interviewService.Get(id);
            if (interviewViewModel == null)
            {
                return NotFound();
            }
            return PartialView(interviewViewModel);
        }


        [HttpGet]
        public IActionResult Edit(int id)
        {

            ViewBag.Title = "Entrevista / Editar";

            if (id == 0)
            {
                return NotFound();
            }

            var viewModel = _interviewService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            var interviewViewModel = _interviewService.Get(id);
            if (interviewViewModel == null)
            {
                return NotFound();
            }
            return View(interviewViewModel);
        }

        [HttpPost]
        public IActionResult Edit(int id, InterviewViewModel interviewViewModel)
        {
            if (id != interviewViewModel.Id)
            {
                return NotFound();
            }

            var viewModel = _interviewService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _interviewService.Update(interviewViewModel);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_interviewService.GetAll().Any(e => e.Id == id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(interviewViewModel);
        }

        [HttpGet]
        public IActionResult TableList()
        {
            return PartialView(_interviewService.GetAll());
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            var viewModel = _interviewService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (_interviewService.CanRemove(id))
            {
                _interviewService.Remove(id);
            }
            else
            {
                //return StatusCode(500, "This item is being used by some Scenario and can not be removed");
                return StatusCode(500, "Este item está sendo usado por algum Cenário e não pode ser removido");
            }


            var indexViewModel = new InterviewIndexViewModel()
            {
                InterviewList = _interviewService.GetAll().ToList(),
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                IsAdmin = isAdmin
            };

            return PartialView(nameof(TableList), indexViewModel);
        }

    }
}
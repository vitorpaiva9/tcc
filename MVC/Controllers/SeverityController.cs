﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces;
using Services.ViewModels;

namespace MVC.Controllers
{
    public class SeverityController : Controller
    {
        private readonly ISeverityService _severityService;


        public SeverityController(ISeverityService severityService)
        {
            _severityService = severityService;
        }

        public IActionResult Index()
        {
            //ViewBag.Title = "Severity";
            ViewBag.Title = "Severidade";

            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;


            var indexViewModel = new SeverityIndexViewModel()
            {
                SeverityList = _severityService.GetAll().ToList(),
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                IsAdmin = isAdmin
            };
            return View(indexViewModel);
        }

        public IActionResult Create()
        {
            //ViewBag.Title = "Severity - Create";
            ViewBag.Title = "Severidade / Criar";
            return View(new SeverityViewModel());
        }

        public IActionResult CreateModal()
        {
            //ViewBag.Title = "Severity - Create";
            ViewBag.Title = "Severidade / Criar";
            return PartialView(nameof(Create), new SeverityViewModel() { FromModal = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SeverityViewModel severityViewModel)
        {

            if (ModelState.IsValid)
            {
                severityViewModel.UserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value);

                _severityService.Add(severityViewModel);

                if (severityViewModel.FromModal)
                {
                    var scenarioConflictViewModel = new ScenarioConflictViewModel();
                    scenarioConflictViewModel.SeverityList = _severityService.GetAll();
                    return PartialView("DropdownSeverity", scenarioConflictViewModel);
                }
                else
                {
                    return RedirectToAction(nameof(Index));
                }
            }

            if (severityViewModel.FromModal)
            {
                Response.StatusCode = 400;
                return PartialView(nameof(Create), severityViewModel);
            }
            else
            {
                return View(severityViewModel);
            }
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var severityViewModel = _severityService.Get(id);
            if (severityViewModel == null)
            {
                return NotFound();
            }
            return PartialView(severityViewModel);
        }


        [HttpGet]
        public IActionResult Edit(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var severityViewModel = _severityService.Get(id);

            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(severityViewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (severityViewModel == null)
            {
                return NotFound();
            }
            return View(severityViewModel);
        }

        [HttpPost]
        public IActionResult Edit(int id, SeverityViewModel severityViewModel)
        {

            ViewBag.Title = "Severidade / Editar";

            if (id != severityViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var viewModel = _severityService.Get(id);
                var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

                var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

                if (!isAdmin && !canEdit)
                {
                    return Unauthorized();
                }


                try
                {
                    _severityService.Update(severityViewModel);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_severityService.GetAll().Any(e => e.Id == id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(severityViewModel);
        }

        [HttpGet]
        public IActionResult TableList()
        {
            return PartialView(_severityService.GetAll());
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            var viewModel = _severityService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (_severityService.CanRemove(id))
            {
                _severityService.Remove(id);
            }
            else
            {
                //return StatusCode(500, "This item is being used by some Scenario and can not be removed");
                return StatusCode(500, "Este item está sendo usado por algum Cenário e não pode ser removido");
            }



            var indexViewModel = new SeverityIndexViewModel()
            {
                SeverityList = _severityService.GetAll().ToList(),
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                IsAdmin = isAdmin
            };

            return PartialView(nameof(TableList), indexViewModel);
        }

    }
}
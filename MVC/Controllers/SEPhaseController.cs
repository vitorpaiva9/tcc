﻿using System;
using System.Linq;
using System.Security.Claims;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces;
using Services.ViewModels;

namespace MVC.Controllers
{
    public class SEPhaseController : Controller
    {
        private readonly ISEPhaseService _sePhaseService;


        public SEPhaseController(ISEPhaseService sePhaseService)
        {
            _sePhaseService = sePhaseService;
        }

        public IActionResult Index()
        {
            //ViewBag.Title = "Software Engineer Phase";
            ViewBag.Title = "Fase da Engenharia de Software";

            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;


            var indexViewModel = new SEPhaseIndexViewModel()
            {
                SEPhaseList = _sePhaseService.GetAll().ToList(),
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                IsAdmin = isAdmin
            };
            return View(indexViewModel);

        }

        public IActionResult Create()
        {
            //ViewBag.Title = "Software Engineer Phase - Create";
            ViewBag.Title = "Fase da Engenharia de Software / Criar";
            return View(new SEPhaseViewModel());
        }

        public IActionResult CreateModal()
        {
            //ViewBag.Title = "Software Engineer Phase - Create";
            ViewBag.Title = "Fase da Engenharia de Software / Criar";
            return PartialView(nameof(Create), new SEPhaseViewModel() { FromModal = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SEPhaseViewModel sePhaseViewModel)
        {
            if (ModelState.IsValid)
            {
                sePhaseViewModel.UserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value);

                _sePhaseService.Add(sePhaseViewModel);

                if (sePhaseViewModel.FromModal)
                {
                    var scenarioConflictViewModel = new ScenarioConflictViewModel();
                    scenarioConflictViewModel.SEPhaseList = _sePhaseService.GetAll();
                    return PartialView("DropdownSEPhase", scenarioConflictViewModel);
                }
                else
                {
                    return RedirectToAction(nameof(Index));
                }
            }

            if (sePhaseViewModel.FromModal)
            {
                Response.StatusCode = 400;
                return PartialView(nameof(Create), sePhaseViewModel);
            }
            else
            {
                return View(sePhaseViewModel);
            }
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var sePhaseViewModel = _sePhaseService.Get(id);
            if (sePhaseViewModel == null)
            {
                return NotFound();
            }
            return PartialView(sePhaseViewModel);
        }


        [HttpGet]
        public IActionResult Edit(int id)
        {

            ViewBag.Title = "Fase da Engenharia de Software / Editar";

            if (id == 0)
            {
                return NotFound();
            }

            var viewModel = _sePhaseService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            var sePhaseViewModel = _sePhaseService.Get(id);
            if (sePhaseViewModel == null)
            {
                return NotFound();
            }
            return View(sePhaseViewModel);
        }

        [HttpPost]
        public IActionResult Edit(int id, SEPhaseViewModel sePhaseViewModel)
        {
            if (id != sePhaseViewModel.Id)
            {
                return NotFound();
            }

            var viewModel = _sePhaseService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _sePhaseService.Update(sePhaseViewModel);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_sePhaseService.GetAll().Any(e => e.Id == id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(sePhaseViewModel);
        }

        [HttpGet]
        public IActionResult TableList()
        {
            return PartialView(_sePhaseService.GetAll());
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            var viewModel = _sePhaseService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (_sePhaseService.CanRemove(id))
            {
                _sePhaseService.Remove(id);
            }
            else
            {
                //return StatusCode(500, "This item is being used by some Scenario and can not be removed");
                return StatusCode(500, "Este item está sendo usado por algum Cenário e não pode ser removido");
            }



            var indexViewModel = new SEPhaseIndexViewModel()
            {
                SEPhaseList = _sePhaseService.GetAll().ToList(),
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                IsAdmin = isAdmin
            };

            return PartialView(nameof(TableList), indexViewModel);
        }
    }
}
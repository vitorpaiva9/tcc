﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces;
using Services.Services;
using Services.ViewModels;

namespace MVC.Controllers
{
    public class CompanyController : Controller
    {
        private readonly ICompanyService _companyService;


        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        public IActionResult Index()
        {
            //ViewBag.Title = "Company";
            ViewBag.Title = "Perfil de Empresa";

            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;


            var indexViewModel = new CompanyIndexViewModel()
            {
                CompanyList = _companyService.GetAll().ToList(),
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                IsAdmin = isAdmin
            };
            return View(indexViewModel);
        }

        public IActionResult Create()
        {
            //ViewBag.Title = "Company - Create";
            ViewBag.Title = "Perfil de Empresa / Criar";
            return View(new CompanyViewModel());
        }

        public IActionResult CreateModal()
        {
            //ViewBag.Title = "Company - Create";
            ViewBag.Title = "Perfil de Empresa / Criar";
            return PartialView(nameof(Create), new CompanyViewModel() { FromModal = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CompanyViewModel companyViewModel)
        {

            if (ModelState.IsValid)
            {
                companyViewModel.UserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value);

                _companyService.Add(companyViewModel);

                if (companyViewModel.FromModal)
                {
                    var scenarioGeneralViewModel = new ScenarioGeneralViewModel();
                    scenarioGeneralViewModel.CompanyList = _companyService.GetAll();
                    return PartialView("DropdownCompany", scenarioGeneralViewModel);
                }
                else
                {
                    return RedirectToAction(nameof(Index));
                }
            }

            if (companyViewModel.FromModal)
            {
                Response.StatusCode = 400;
                return PartialView(nameof(Create), companyViewModel);
            }
            else
            {
                return View(companyViewModel);
            }
        }

        public IActionResult Details(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var companyViewModel = _companyService.Get(id);
            if (companyViewModel == null)
            {
                return NotFound();
            }
            return PartialView(companyViewModel);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            ViewBag.Title = "Perfil de Empresa / Editar";

            if (id == 0)
            {
                return NotFound();
            }

            var viewModel = _companyService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            var companyViewModel = _companyService.Get(id);
            if (companyViewModel == null)
            {
                return NotFound();
            }
            return View(companyViewModel);
        }

        [HttpPost]
        public IActionResult Edit(int id, CompanyViewModel companyViewModel)
        {
            if (id != companyViewModel.Id)
            {
                return NotFound();
            }

            var viewModel = _companyService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _companyService.Update(companyViewModel);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_companyService.GetAll().Any(e => e.Id == id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(companyViewModel);
        }

        [HttpGet]
        public IActionResult TableList()
        {
            return PartialView(_companyService.GetAll());
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {

            var viewModel = _companyService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (_companyService.CanRemove(id))
            {
                _companyService.Remove(id);
            }
            else
            {
                //return StatusCode(500, "This item is being used by some Scenario and can not be removed");
                return StatusCode(500, "Este item está sendo usado por algum Cenário e não pode ser removido");
            }



            var indexViewModel = new CompanyIndexViewModel()
            {
                CompanyList = _companyService.GetAll().ToList(),
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                IsAdmin = isAdmin
            };

            return PartialView(nameof(TableList), indexViewModel);
        }
    }
}
﻿using System;
using System.Linq;
using System.Security.Claims;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces;
using Services.ViewModels;

namespace MVC.Controllers
{
    public class ConflictTypeController : Controller
    {
        private readonly IConflictTypeService _conflictTypeService;


        public ConflictTypeController(IConflictTypeService conflictTypeService)
        {
            _conflictTypeService = conflictTypeService;
        }

        public IActionResult Index()
        {
            //ViewBag.Title = "Conflict Type";
            ViewBag.Title = "Tipo de Conflito";

            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;


            var indexViewModel = new ConflictTypeIndexViewModel()
            {
                ConflictTypeList = _conflictTypeService.GetAll().ToList(),
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                IsAdmin = isAdmin
            };
            return View(indexViewModel);
        }

        public IActionResult Create()
        {
            //ViewBag.Title = "Conflict Type - Create";
            ViewBag.Title = "Tipo de Conflito / Criar";
            return View(new ConflictTypeViewModel());
        }

        public IActionResult CreateModal()
        {
            //ViewBag.Title = "Conflict Types - Create";
            ViewBag.Title = "Tipo de Conflito / Criar";
            return PartialView(nameof(Create), new ConflictTypeViewModel() { FromModal = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ConflictTypeViewModel conflictTypeViewModel)
        {
            if (ModelState.IsValid)
            {
                conflictTypeViewModel.UserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value);

                _conflictTypeService.Add(conflictTypeViewModel);

                if (conflictTypeViewModel.FromModal)
                {
                    var scenarioConflictViewModel = new ScenarioConflictViewModel();
                    scenarioConflictViewModel.ConflictTypeList = _conflictTypeService.GetAll();
                    return PartialView("DropdownConflictType", scenarioConflictViewModel);
                }
                else
                {
                    return RedirectToAction(nameof(Index));
                }
            }

            if (conflictTypeViewModel.FromModal)
            {
                Response.StatusCode = 400;
                return PartialView(nameof(Create), conflictTypeViewModel);
            }
            else
            {
                return View(conflictTypeViewModel);
            }
        }

        public IActionResult Details(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var conflictTypeViewModel = _conflictTypeService.Get(id);
            if (conflictTypeViewModel == null)
            {
                return NotFound();
            }
            return PartialView(conflictTypeViewModel);
        }


        [HttpGet]
        public IActionResult Edit(int id)
        {

            ViewBag.Title = "Tipo de Conflito / Editar";

            if (id == 0)
            {
                return NotFound();
            }

            var viewModel = _conflictTypeService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            var conflictTypeViewModel = _conflictTypeService.Get(id);
            if (conflictTypeViewModel == null)
            {
                return NotFound();
            }
            return View(conflictTypeViewModel);
        }

        [HttpPost]
        public IActionResult Edit(int id, ConflictTypeViewModel conflictTypeViewModel)
        {
            if (id != conflictTypeViewModel.Id)
            {
                return NotFound();
            }

            var viewModel = _conflictTypeService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _conflictTypeService.Update(conflictTypeViewModel);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_conflictTypeService.GetAll().Any(e => e.Id == id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(conflictTypeViewModel);
        }

        [HttpGet]
        public IActionResult TableList()
        {
            return PartialView(_conflictTypeService.GetAll());
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {

            var viewModel = _conflictTypeService.Get(id);
            var isAdmin = (Roles)Enum.Parse(typeof(Roles), HttpContext.User.FindFirst(ClaimTypes.Role).Value) == Roles.Admin;

            var canEdit = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value).Equals(viewModel.UserId.Value);

            if (!isAdmin && !canEdit)
            {
                return Unauthorized();
            }

            if (_conflictTypeService.CanRemove(id))
            {
                _conflictTypeService.Remove(id);
            }
            else
            {
                //return StatusCode(500, "This item is being used by some Scenario and can not be removed");
                return StatusCode(500, "Este item está sendo usado por algum Cenário e não pode ser removido");
            }

            var indexViewModel = new ConflictTypeIndexViewModel()
            {
                ConflictTypeList = _conflictTypeService.GetAll().ToList(),
                CurrentUserId = int.Parse(HttpContext.User.FindFirst(ClaimTypes.Hash).Value),
                IsAdmin = isAdmin
            };
            return PartialView(nameof(TableList), indexViewModel);
        }

    }
}
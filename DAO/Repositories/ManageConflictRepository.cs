﻿using DAO.Interfaces;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Repositories
{
    public class ManageConflictRepository : BaseRepository<ManageConflict>, IManageConflictRepository
    {
        public ManageConflictRepository(DataBaseContext context) : base(context)
        {
            _context = context;
        }
    }
}

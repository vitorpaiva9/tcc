﻿using Domain.Entities;
using DAO.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Repositories
{
    public class SeverityRepository : BaseRepository<Severity>, ISeverityRepository
    {
        public SeverityRepository(DataBaseContext context) : base(context)
        {
            _context = context;
        }
    }
}

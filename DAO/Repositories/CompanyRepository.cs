﻿using DAO.Interfaces;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Repositories
{
    public class CompanyRepository : BaseRepository<Company> , ICompanyRepository
    {
        public CompanyRepository(DataBaseContext context) : base(context)
        {
            _context = context;
        }
    }
}

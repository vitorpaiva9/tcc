﻿using DAO.Interfaces;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DAO.Repositories
{
    public abstract class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        public DataBaseContext _context;
        public DbSet<T> _dbSet;

        public BaseRepository(DataBaseContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public virtual T Get(int id)
        {
            return _dbSet.FirstOrDefault(x => x.Id == id);
        }

        public virtual void Remove(int id)
        {
            T entity = _context.Set<T>().Find(id);
            _dbSet.Remove(entity);
            _context.SaveChanges();
        }

        public virtual T Add(T entity)
        {
            _dbSet.Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public virtual T Update(T entity)
        {
            _dbSet.Update(entity);
            _context.SaveChanges();
            return entity;
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _dbSet;
        }
    }
}

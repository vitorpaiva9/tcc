﻿using DAO.Interfaces;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Repositories
{
    public class SEPhaseRepository : BaseRepository<SEPhase>, ISEPhaseRepository
    {
        public SEPhaseRepository(DataBaseContext context) : base(context)
        {
            _context = context;
        }
    }
}

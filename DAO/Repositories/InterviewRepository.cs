﻿using DAO.Interfaces;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Repositories
{
    public class InterviewRepository : BaseRepository<Interview>, IInterviewRepository
    {
        public InterviewRepository(DataBaseContext context) : base(context)
        {
            _context = context;
        }
    }
}

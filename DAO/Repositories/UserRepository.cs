﻿using DAO.Interfaces;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(DataBaseContext context) : base(context)
        {
            _context = context;
        }
    }
}

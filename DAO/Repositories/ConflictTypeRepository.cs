﻿using DAO.Interfaces;
using Domain.Entities;

namespace DAO.Repositories
{
    public class ConflictTypeRepository : BaseRepository<ConflictType>, IConflictTypeRepository
    {
        public ConflictTypeRepository(DataBaseContext context) : base(context)
        {
            _context = context;
        }
    }
}

﻿using DAO.Interfaces;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DAO.Repositories
{
    public class ScenarioRepository : BaseRepository<Scenario>, IScenarioRepository
    {
        public ScenarioRepository(DataBaseContext context) : base(context)
        {
            _context = context;
        }

        public override Scenario Get(int id)
        {
            return _dbSet
                .Include(x => x.ScenarioConflictType)
                .Include(x => x.ScenarioSEPhase)
                .Include(x => x.Severity)
                .Include(x => x.ScenarioManageConflict)
                .Include(x => x.ScenarioEffectConsequence)
                .Include(x => x.Administration)
                .Include(x => x.Climax)
                .Include(x => x.Consequence)
                .Include(x => x.Context)
                .Include(x => x.Perception)
                .Include(x => x.Evolutions)
                .Include(x => x.Stakeholders)
                .ThenInclude(y => y.StakeholderEmpathyMap)
                .ThenInclude(z => z.EmpathyMap)                
                .FirstOrDefault(x => x.Id == id);
        }

        public override IEnumerable<Scenario> GetAll()
        {
            return _dbSet
                .Include(x => x.ScenarioConflictType)
                .Include(x => x.ScenarioSEPhase)
                .Include(x => x.Severity)
                .Include(x => x.ScenarioManageConflict)
                .Include(x => x.ScenarioEffectConsequence)
                .Include(x => x.Administration)
                .Include(x => x.Climax)
                .Include(x => x.Consequence)
                .Include(x => x.Context)
                .Include(x => x.Perception)
                .Include(x => x.Stakeholders)
                .ThenInclude(y => y.StakeholderEmpathyMap)
                .ThenInclude(z => z.EmpathyMap);
        }

        public void RemoveStakeholder(int id)
        {
            var stakeholder =_context.Set<Stakeholder>().Find(id);
            if (stakeholder != null)
            {
                _context.Set<Stakeholder>().Remove(stakeholder);
                _context.SaveChanges();

            }
        }
    }
}

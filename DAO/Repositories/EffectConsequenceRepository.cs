﻿using DAO.Interfaces;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Repositories
{
    public class EffectConsequenceRepository : BaseRepository<EffectConsequence>, IEffectConsequenceRepository
    {
        public EffectConsequenceRepository(DataBaseContext context) : base(context)
        {
            _context = context;
        }
    }
}

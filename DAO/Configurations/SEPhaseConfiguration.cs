﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations
{
    public class SEPhaseConfiguration : BaseConfiguration<SEPhase>
    {
        public override void Configure(EntityTypeBuilder<SEPhase> builder)
        {
            base.Configure(builder);

            builder.ToTable("se_phase");

            builder.Property(x => x.UserId)
                .HasColumnName("user_id")
                .IsRequired(false);

            builder.Property(x => x.Phase)
                .HasColumnName("phase")
                .HasColumnType("text");

        }
    }
}

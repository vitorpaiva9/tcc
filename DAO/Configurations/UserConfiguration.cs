﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations
{
    public class UserConfiguration : BaseConfiguration<User>
    {
        public override void Configure(EntityTypeBuilder<User> builder)
        {
            base.Configure(builder);

            builder.ToTable("users");

            builder.Property(x => x.Username)
                .HasColumnName("username")
                .HasColumnType("text");

            builder.Property(x => x.Password)
                .HasColumnName("password")
                .HasColumnType("text");

            builder.Property(x => x.Role)
                .HasColumnName("role")
                .HasColumnType("integer");

            builder.HasMany(x => x.Scenarios)
                .WithOne(x => x.User);

        }
    }
}

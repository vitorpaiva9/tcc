﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations
{
    public class ScenarioConfiguration : BaseConfiguration<Scenario>
    {
        public override void Configure(EntityTypeBuilder<Scenario> builder)
        {
            base.Configure(builder);

            builder.ToTable("scenario");


            builder.Property(x => x.Summary)
                .HasColumnName("summary")
                .HasColumnType("text")
                .IsRequired(false);

            builder.Property(x => x.TranscriptionPath)
                .HasColumnName("transcription_path")
                .HasColumnType("text")
                .IsRequired(false);

            builder.Property(x => x.ProblemSummary)
                .HasColumnName("problem_summary")
                .HasColumnType("text")
                .IsRequired(false);


            builder.Property(x => x.SeverityId)
                .HasColumnName("severity_id")
                .IsRequired(false);

            builder.Property(x => x.InterviewId)
                .HasColumnName("interview_id")
                .IsRequired(false);

            builder.Property(x => x.UserId)
                .HasColumnName("user_id")
                .IsRequired(false);

            builder.Property(x => x.ScenarioOrigin)
                .HasColumnName("scenario_origin_id")
                .IsRequired(false);

            builder.Property(x => x.Level)
                .HasColumnName("level_id")
                .IsRequired(false);

            builder.Property(x => x.Origin)
                .HasColumnName("origin_id")
                .IsRequired(false);

            builder.Property(x => x.DevelopingType)
                .HasColumnName("developing_type")
                .IsRequired(false);

            builder.Property(x => x.TeamLocalization)
                .HasColumnName("team_localization")
                .IsRequired(false);

            builder.Property(x => x.DescriptionStakeholder1)
                .HasColumnName("description_stakeholder_1")
                .IsRequired(false);

            builder.Property(x => x.DescriptionStakeholder2)
                .HasColumnName("description_stakeholder_2")
                .IsRequired(false);

            builder.Property(x => x.CompanyId)
                .HasColumnName("company_id")
                .IsRequired(false);

            builder.Property(x => x.StakeholderRelatorId)
                .HasColumnName("stakeholder_relator_id")
                .IsRequired(false);

            builder.HasMany(x => x.Stakeholders)
                .WithOne(x => x.Scenario)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);



            builder.Property(x => x.ContextId)
                .HasColumnName("context_id")
                .IsRequired(false);

            builder.Property(x => x.PerceptionId)
                .HasColumnName("perception_id")
                .IsRequired(false);

            builder.Property(x => x.ClimaxId)
                .HasColumnName("climax_id")
                .IsRequired(false);

            builder.Property(x => x.AdministrationId)
                .HasColumnName("administration_id")
                .IsRequired(false);

            builder.Property(x => x.ConsequenceId)
                .HasColumnName("consequence_id")
                .IsRequired(false);


            builder.HasMany(x => x.Evolutions)
                .WithOne(x => x.Scenario)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);



        }
    }
}

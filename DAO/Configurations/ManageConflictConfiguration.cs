﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations
{
    public class ManageConflictConfiguration : BaseConfiguration<ManageConflict>
    {
        public override void Configure(EntityTypeBuilder<ManageConflict> builder)
        {
            base.Configure(builder);

            builder.ToTable("manage_conflict");

            builder.Property(x => x.Style)
                .HasColumnName("style")
                .HasColumnType("text");

            builder.Property(x => x.Description)
                .HasColumnName("description")
                .HasColumnType("text");

            builder.Property(x => x.Effect)
                .HasColumnName("effect")
                .HasColumnType("text");

            builder.Property(x => x.UserId)
                .HasColumnName("user_id")
                .IsRequired(false);

        }
    }
}

﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations
{
    public class ConflictJourneyConfiguration : BaseConfiguration<ConflictJourney>
    {
        public override void Configure(EntityTypeBuilder<ConflictJourney> builder)
        {
            base.Configure(builder);

            builder.ToTable("conflict_journey");

            //builder.Property(x => x.BehaviorStakeholder1)
            //    .HasColumnName("behavior_stakeholder_1");

            //builder.Property(x => x.BehaviorStakeholder2)
            //    .HasColumnName("behavior_stakeholder_2");

            //builder.Property(x => x.FeelingStakeholder1)
            //    .HasColumnName("behavior_stakeholder_1");

            //builder.Property(x => x.FeelingStakeholder2)
            //    .HasColumnName("behavior_stakeholder_2");

            //builder.Property(x => x.Stage)
            //    .HasColumnName("stage")
            //    .HasColumnType("text");



            //builder.Property(x => x.ScenarioId)
            //    .HasColumnName("scenario_id")
            //    .HasColumnType("integer");




        }
    }
}

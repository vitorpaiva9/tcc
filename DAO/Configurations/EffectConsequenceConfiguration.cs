﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations
{
    public class EffectConsequenceConfiguration : BaseConfiguration<EffectConsequence>
    {
        public override void Configure(EntityTypeBuilder<EffectConsequence> builder)
        {
            base.Configure(builder);

            builder.ToTable("effect_consequence");

            builder.Property(x => x.UserId)
                .HasColumnName("user_id")
                .IsRequired(false);

            builder.Property(x => x.EffectAndConsequence)
                .HasColumnName("effect_consequence")
                .HasColumnType("text");

        }
    }
}

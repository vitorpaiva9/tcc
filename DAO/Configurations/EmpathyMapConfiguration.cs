﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations
{
    public class EmpathyMapConfiguration : BaseConfiguration<EmpathyMap>
    {
        public override void Configure(EntityTypeBuilder<EmpathyMap> builder)
        {
            base.Configure(builder);

            builder.ToTable("empathy_map");

            builder.Property(x => x.EmpathyMapType)
                .HasColumnName("empathy_map_type")
                .HasColumnType("text");

            builder.Property(x => x.Description)
                .HasColumnName("description")
                .HasColumnType("text");


        }
    }
}
﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations
{
    public class StakeholderConfiguration : BaseConfiguration<Stakeholder>
    {
        public override void Configure(EntityTypeBuilder<Stakeholder> builder)
        {
            base.Configure(builder);

            builder.ToTable("stakeholder");

            builder.Property(x => x.Description)
                .HasColumnName("description");

            builder.Property(x => x.Type)
                .HasColumnName("type");


            builder.Property(x => x.ScenarioId)
                .HasColumnName("scenario_id")
                .HasColumnType("integer");

            builder.Property(x => x.UserId)
                .HasColumnName("user_id")
                .IsRequired(false);


        }
    }
}

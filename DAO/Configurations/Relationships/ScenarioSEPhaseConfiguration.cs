﻿using Domain.Entities.Relationships;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations.Relationships
{
    public class ScenarioSEPhaseConfiguration : BaseConfiguration<ScenarioSEPhase>
    {
        public override void Configure(EntityTypeBuilder<ScenarioSEPhase> builder)
        {
            base.Configure(builder);

            builder.ToTable("scenario_se_phase");


            builder.Property(x => x.SEPhaseId)
                .HasColumnName("se_phase_id")
                .HasColumnType("integer");

            builder.Property(x => x.ScenarioId)
                .HasColumnName("scenario_id")
                .HasColumnType("integer");

            builder.HasOne(x => x.Scenario)
                .WithMany(x => x.ScenarioSEPhase)
                .HasForeignKey(x => x.ScenarioId);

            builder.HasOne(x => x.SEPhase)
                .WithMany(x => x.ScenarioSEPhase)
                .HasForeignKey(x => x.SEPhaseId);
        }
    }
}

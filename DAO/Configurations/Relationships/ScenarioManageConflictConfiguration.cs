﻿using Domain.Entities.Relationships;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations.Relationships
{
    public class ScenarioManageConflictConfiguration : BaseConfiguration<ScenarioManageConflict>
    {
        public override void Configure(EntityTypeBuilder<ScenarioManageConflict> builder)
        {
            base.Configure(builder);

            builder.ToTable("scenario_manage_conflict");


            builder.Property(x => x.ManageConflictId)
                .HasColumnName("manage_conflict_id")
                .HasColumnType("integer");

            builder.Property(x => x.ScenarioId)
                .HasColumnName("scenario_id")
                .HasColumnType("integer");

            builder.HasOne(x => x.Scenario)
                .WithMany(x => x.ScenarioManageConflict)
                .HasForeignKey(x => x.ScenarioId);

            builder.HasOne(x => x.ManageConflict)
                .WithMany(x => x.ScenarioManageConflict)
                .HasForeignKey(x => x.ManageConflictId);
        }
    }
}

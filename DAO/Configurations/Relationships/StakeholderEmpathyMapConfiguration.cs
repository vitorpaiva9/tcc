﻿using Domain.Entities.Relationships;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations.Relationships
{
    public class StakeholderEmpathyMapConfiguration : BaseConfiguration<StakeholderEmpathyMap>
    {
        public override void Configure(EntityTypeBuilder<StakeholderEmpathyMap> builder)
        {
            base.Configure(builder);

            builder.ToTable("stakeholder_empathy_map");


            builder.Property(x => x.StakeholderId)
                .HasColumnName("stakeholder_id")
                .HasColumnType("integer");

            builder.Property(x => x.EmpathyMapId)
                .HasColumnName("empathy_map_id")
                .HasColumnType("integer");

            builder.HasOne(x => x.Stakeholder)
                .WithMany(x => x.StakeholderEmpathyMap)
                .HasForeignKey(x => x.StakeholderId);

            builder.HasOne(x => x.EmpathyMap)
                .WithMany(x => x.StakeholderEmpathyMap)
                .HasForeignKey(x => x.EmpathyMapId);
        }
    }
}

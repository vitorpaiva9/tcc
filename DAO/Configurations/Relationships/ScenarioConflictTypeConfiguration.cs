﻿using Domain.Entities.Relationships;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAO.Configurations.Relationships
{
    public class ScenarioConflictTypeConfiguration : BaseConfiguration<ScenarioConflictType>
    {
        public override void Configure(EntityTypeBuilder<ScenarioConflictType> builder)
        {
            base.Configure(builder);

            builder.ToTable("scenario_conflict_type");


            builder.Property(x => x.ConflictTypeId)
                .HasColumnName("conflict_type_id")
                .HasColumnType("integer");

            builder.Property(x => x.ScenarioId)
                .HasColumnName("scenario_id")
                .HasColumnType("integer");

            builder.HasOne(x => x.Scenario)
                .WithMany(x => x.ScenarioConflictType)
                .HasForeignKey(x => x.ScenarioId);

            builder.HasOne(x => x.ConflictType)
                .WithMany(x => x.ScenarioConflictType)
                .HasForeignKey(x => x.ConflictTypeId);
        }
    }

}

﻿using Domain.Entities.Relationships;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations.Relationships
{
    class ScenarioEffectConsequenceConfiguration : BaseConfiguration<ScenarioEffectConsequence>
    {
        public override void Configure(EntityTypeBuilder<ScenarioEffectConsequence> builder)
        {
            base.Configure(builder);

            builder.ToTable("scenario_effect_consequence");


            builder.Property(x => x.EffectConsequenceId)
                .HasColumnName("effect_consequence_id")
                .HasColumnType("integer");

            builder.Property(x => x.ScenarioId)
                .HasColumnName("scenario_id")
                .HasColumnType("integer");

            builder.HasOne(x => x.Scenario)
                .WithMany(x => x.ScenarioEffectConsequence)
                .HasForeignKey(x => x.ScenarioId);

            builder.HasOne(x => x.EffectConsequence)
                .WithMany(x => x.ScenarioEffectConsequence)
                .HasForeignKey(x => x.EffectConsequenceId);
        }
    }
}

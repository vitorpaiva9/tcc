﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations
{
    public class SeverityConfiguration : BaseConfiguration<Severity>
    {
        public override void Configure(EntityTypeBuilder<Severity> builder)
        {
            base.Configure(builder);

            builder.ToTable("severity");

            builder.Property(x => x.Type)
                .HasColumnName("type")
                .HasColumnType("text");

            builder.Property(x => x.Level)
                .HasColumnName("level")
                .HasColumnType("integer");

            builder.Property(x => x.UserId)
                .HasColumnName("user_id")
                .IsRequired(false);

            builder.HasMany(x => x.Scenarios)
                .WithOne(x => x.Severity);

        }
    }
}

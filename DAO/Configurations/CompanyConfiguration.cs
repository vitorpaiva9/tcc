﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations
{
    public class CompanyConfiguration : BaseConfiguration<Company>
    {
        public override void Configure(EntityTypeBuilder<Company> builder)
        {
            base.Configure(builder);

            builder.ToTable("company");

            builder.Property(x => x.Description)
                .HasColumnName("description")
                .HasColumnType("text");

            builder.Property(x => x.PublicOrPrivate)
                .HasColumnName("public_or_private");

            builder.Property(x => x.Type)
                .HasColumnName("type")
                .HasColumnType("text");

            builder.Property(x => x.EmployeesNumber)
                .HasColumnName("employees_number")
                .HasColumnType("integer");

            builder.Property(x => x.UserId)
                .HasColumnName("user_id")
                .IsRequired(false);

            builder.HasMany(x => x.Scenarios)
                .WithOne(x => x.Company)
                .IsRequired(false);

        }
    }
}

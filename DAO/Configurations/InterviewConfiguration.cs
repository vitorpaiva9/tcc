﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations
{
    public class InterviewConfiguration : BaseConfiguration<Interview>
    {
        public override void Configure(EntityTypeBuilder<Interview> builder)
        {
            base.Configure(builder);

            builder.ToTable("interview");

            builder.Property(x => x.Realization)
                .HasColumnName("realization")
                .HasColumnType("date");

            builder.Property(x => x.Title)
                .HasColumnName("title")
                .HasColumnType("text");

            builder.Property(x => x.Interviewer)
                .HasColumnName("interviewer")
                .HasColumnType("text");

            builder.Property(x => x.UserId)
                .HasColumnName("user_id")
                .IsRequired(false);

            builder.HasMany(x => x.Scenarios)
                .WithOne(x => x.Interview)
                .IsRequired(false);

        }
    }
}
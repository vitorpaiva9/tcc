﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Configurations
{
    public class ConflictTypeConfiguration : BaseConfiguration<ConflictType>
    {
        public override void Configure(EntityTypeBuilder<ConflictType> builder)
        {
            base.Configure(builder);

            builder.ToTable("conflict_type");

            builder.Property(x => x.UserId)
                .HasColumnName("user_id")
                .IsRequired(false);

            builder.Property(x => x.Type)
                .HasColumnName("type")
                .HasColumnType("text");

        }
    }
}

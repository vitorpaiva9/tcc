﻿using DAO.Repositories;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Interfaces
{
    public interface IManageConflictRepository : IRepository<ManageConflict>
    {
    }
}

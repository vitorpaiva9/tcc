﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Interfaces
{
    public interface IScenarioRepository : IRepository<Scenario>
    {
        void RemoveStakeholder(int id);
    }
}

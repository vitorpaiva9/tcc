﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Interfaces
{
    public interface ISeverityRepository : IRepository<Severity>
    {
    }
}

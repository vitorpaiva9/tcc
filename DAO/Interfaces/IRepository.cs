﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.Interfaces
{
    public interface IRepository<T> where T : class
    {
        T Get(int id);
        T Update(T entity);
        T Add(T entity);
        void Remove(int id);
        IEnumerable<T> GetAll();
    }
}

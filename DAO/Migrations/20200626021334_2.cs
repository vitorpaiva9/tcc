﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAO.Migrations
{
    public partial class _2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "developing_type",
                table: "scenario",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "team_localization",
                table: "scenario",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "developing_type",
                table: "scenario");

            migrationBuilder.DropColumn(
                name: "team_localization",
                table: "scenario");
        }
    }
}

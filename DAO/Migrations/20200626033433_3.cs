﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAO.Migrations
{
    public partial class _3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "description_stakeholder_1",
                table: "scenario",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "description_stakeholder_2",
                table: "scenario",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "description_stakeholder_1",
                table: "scenario");

            migrationBuilder.DropColumn(
                name: "description_stakeholder_2",
                table: "scenario");
        }
    }
}

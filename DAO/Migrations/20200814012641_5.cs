﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAO.Migrations
{
    public partial class _5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ConflictImpact",
                table: "scenario",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "scenario",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConflictImpact",
                table: "scenario");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "scenario");
        }
    }
}

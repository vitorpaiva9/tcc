﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DAO.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "empathy_map",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    empathy_map_type = table.Column<string>(type: "text", nullable: false),
                    description = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_empathy_map", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    username = table.Column<string>(type: "text", nullable: true),
                    password = table.Column<string>(type: "text", nullable: true),
                    role = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "company",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    description = table.Column<string>(type: "text", nullable: true),
                    public_or_private = table.Column<int>(nullable: false),
                    type = table.Column<string>(type: "text", nullable: true),
                    employees_number = table.Column<int>(type: "integer", nullable: false),
                    user_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_company", x => x.id);
                    table.ForeignKey(
                        name: "FK_company_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "conflict_type",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    type = table.Column<string>(type: "text", nullable: true),
                    user_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_conflict_type", x => x.id);
                    table.ForeignKey(
                        name: "FK_conflict_type_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "effect_consequence",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    effect_consequence = table.Column<string>(type: "text", nullable: true),
                    user_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_effect_consequence", x => x.id);
                    table.ForeignKey(
                        name: "FK_effect_consequence_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "interview",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    title = table.Column<string>(type: "text", nullable: true),
                    interviewer = table.Column<string>(type: "text", nullable: true),
                    realization = table.Column<DateTime>(type: "date", nullable: false),
                    user_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_interview", x => x.id);
                    table.ForeignKey(
                        name: "FK_interview_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "manage_conflict",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    style = table.Column<string>(type: "text", nullable: true),
                    description = table.Column<string>(type: "text", nullable: true),
                    effect = table.Column<string>(type: "text", nullable: true),
                    user_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_manage_conflict", x => x.id);
                    table.ForeignKey(
                        name: "FK_manage_conflict_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "se_phase",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    phase = table.Column<string>(type: "text", nullable: true),
                    user_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_se_phase", x => x.id);
                    table.ForeignKey(
                        name: "FK_se_phase_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "severity",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    type = table.Column<string>(type: "text", nullable: true),
                    level = table.Column<int>(type: "integer", nullable: false),
                    user_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_severity", x => x.id);
                    table.ForeignKey(
                        name: "FK_severity_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "scenario",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    summary = table.Column<string>(type: "text", nullable: true),
                    transcription_path = table.Column<string>(type: "text", nullable: true),
                    problem_summary = table.Column<string>(type: "text", nullable: true),
                    scenario_origin_id = table.Column<int>(nullable: true),
                    level_id = table.Column<int>(nullable: true),
                    origin_id = table.Column<int>(nullable: true),
                    severity_id = table.Column<int>(nullable: true),
                    company_id = table.Column<int>(nullable: true),
                    interview_id = table.Column<int>(nullable: true),
                    user_id = table.Column<int>(nullable: true),
                    stakeholder_relator_id = table.Column<int>(nullable: true),
                    context_id = table.Column<int>(nullable: true),
                    ContextId1 = table.Column<int>(nullable: true),
                    perception_id = table.Column<int>(nullable: true),
                    PerceptionId1 = table.Column<int>(nullable: true),
                    climax_id = table.Column<int>(nullable: true),
                    ClimaxId1 = table.Column<int>(nullable: true),
                    administration_id = table.Column<int>(nullable: true),
                    AdministrationId1 = table.Column<int>(nullable: true),
                    consequence_id = table.Column<int>(nullable: true),
                    ConsequenceId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_scenario", x => x.id);
                    table.ForeignKey(
                        name: "FK_scenario_company_company_id",
                        column: x => x.company_id,
                        principalTable: "company",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_scenario_interview_interview_id",
                        column: x => x.interview_id,
                        principalTable: "interview",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_scenario_severity_severity_id",
                        column: x => x.severity_id,
                        principalTable: "severity",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_scenario_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "conflict_journey",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    FeelingStakeholder1 = table.Column<int>(nullable: false),
                    BehaviorStakeholder1 = table.Column<int>(nullable: false),
                    Stage = table.Column<string>(nullable: true),
                    FeelingStakeholder2 = table.Column<int>(nullable: false),
                    BehaviorStakeholder2 = table.Column<int>(nullable: false),
                    ScenarioId1 = table.Column<int>(nullable: true),
                    ScenarioId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_conflict_journey", x => x.id);
                    table.ForeignKey(
                        name: "FK_conflict_journey_scenario_ScenarioId1",
                        column: x => x.ScenarioId1,
                        principalTable: "scenario",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_conflict_journey_users_UserId",
                        column: x => x.UserId,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "scenario_conflict_type",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    scenario_id = table.Column<int>(type: "integer", nullable: false),
                    conflict_type_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_scenario_conflict_type", x => x.id);
                    table.ForeignKey(
                        name: "FK_scenario_conflict_type_conflict_type_conflict_type_id",
                        column: x => x.conflict_type_id,
                        principalTable: "conflict_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_scenario_conflict_type_scenario_scenario_id",
                        column: x => x.scenario_id,
                        principalTable: "scenario",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "scenario_effect_consequence",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    scenario_id = table.Column<int>(type: "integer", nullable: false),
                    effect_consequence_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_scenario_effect_consequence", x => x.id);
                    table.ForeignKey(
                        name: "FK_scenario_effect_consequence_effect_consequence_effect_conse~",
                        column: x => x.effect_consequence_id,
                        principalTable: "effect_consequence",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_scenario_effect_consequence_scenario_scenario_id",
                        column: x => x.scenario_id,
                        principalTable: "scenario",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "scenario_manage_conflict",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    scenario_id = table.Column<int>(type: "integer", nullable: false),
                    manage_conflict_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_scenario_manage_conflict", x => x.id);
                    table.ForeignKey(
                        name: "FK_scenario_manage_conflict_manage_conflict_manage_conflict_id",
                        column: x => x.manage_conflict_id,
                        principalTable: "manage_conflict",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_scenario_manage_conflict_scenario_scenario_id",
                        column: x => x.scenario_id,
                        principalTable: "scenario",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "scenario_se_phase",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    scenario_id = table.Column<int>(type: "integer", nullable: false),
                    se_phase_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_scenario_se_phase", x => x.id);
                    table.ForeignKey(
                        name: "FK_scenario_se_phase_se_phase_se_phase_id",
                        column: x => x.se_phase_id,
                        principalTable: "se_phase",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_scenario_se_phase_scenario_scenario_id",
                        column: x => x.scenario_id,
                        principalTable: "scenario",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "stakeholder",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    description = table.Column<int>(nullable: false),
                    type = table.Column<int>(nullable: false),
                    ScenarioId1 = table.Column<int>(nullable: true),
                    scenario_id = table.Column<int>(type: "integer", nullable: false),
                    user_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stakeholder", x => x.id);
                    table.ForeignKey(
                        name: "FK_stakeholder_scenario_ScenarioId1",
                        column: x => x.ScenarioId1,
                        principalTable: "scenario",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_stakeholder_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "stakeholder_empathy_map",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    stakeholder_id = table.Column<int>(type: "integer", nullable: false),
                    empathy_map_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stakeholder_empathy_map", x => x.id);
                    table.ForeignKey(
                        name: "FK_stakeholder_empathy_map_empathy_map_empathy_map_id",
                        column: x => x.empathy_map_id,
                        principalTable: "empathy_map",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_stakeholder_empathy_map_stakeholder_stakeholder_id",
                        column: x => x.stakeholder_id,
                        principalTable: "stakeholder",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_company_user_id",
                table: "company",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_conflict_journey_ScenarioId1",
                table: "conflict_journey",
                column: "ScenarioId1");

            migrationBuilder.CreateIndex(
                name: "IX_conflict_journey_UserId",
                table: "conflict_journey",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_conflict_type_user_id",
                table: "conflict_type",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_effect_consequence_user_id",
                table: "effect_consequence",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_interview_user_id",
                table: "interview",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_manage_conflict_user_id",
                table: "manage_conflict",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_AdministrationId1",
                table: "scenario",
                column: "AdministrationId1");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_ClimaxId1",
                table: "scenario",
                column: "ClimaxId1");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_company_id",
                table: "scenario",
                column: "company_id");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_ConsequenceId1",
                table: "scenario",
                column: "ConsequenceId1");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_ContextId1",
                table: "scenario",
                column: "ContextId1");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_interview_id",
                table: "scenario",
                column: "interview_id");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_PerceptionId1",
                table: "scenario",
                column: "PerceptionId1");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_severity_id",
                table: "scenario",
                column: "severity_id");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_stakeholder_relator_id",
                table: "scenario",
                column: "stakeholder_relator_id");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_user_id",
                table: "scenario",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_conflict_type_conflict_type_id",
                table: "scenario_conflict_type",
                column: "conflict_type_id");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_conflict_type_scenario_id",
                table: "scenario_conflict_type",
                column: "scenario_id");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_effect_consequence_effect_consequence_id",
                table: "scenario_effect_consequence",
                column: "effect_consequence_id");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_effect_consequence_scenario_id",
                table: "scenario_effect_consequence",
                column: "scenario_id");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_manage_conflict_manage_conflict_id",
                table: "scenario_manage_conflict",
                column: "manage_conflict_id");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_manage_conflict_scenario_id",
                table: "scenario_manage_conflict",
                column: "scenario_id");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_se_phase_se_phase_id",
                table: "scenario_se_phase",
                column: "se_phase_id");

            migrationBuilder.CreateIndex(
                name: "IX_scenario_se_phase_scenario_id",
                table: "scenario_se_phase",
                column: "scenario_id");

            migrationBuilder.CreateIndex(
                name: "IX_se_phase_user_id",
                table: "se_phase",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_severity_user_id",
                table: "severity",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_stakeholder_ScenarioId1",
                table: "stakeholder",
                column: "ScenarioId1");

            migrationBuilder.CreateIndex(
                name: "IX_stakeholder_user_id",
                table: "stakeholder",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_stakeholder_empathy_map_empathy_map_id",
                table: "stakeholder_empathy_map",
                column: "empathy_map_id");

            migrationBuilder.CreateIndex(
                name: "IX_stakeholder_empathy_map_stakeholder_id",
                table: "stakeholder_empathy_map",
                column: "stakeholder_id");

            migrationBuilder.AddForeignKey(
                name: "FK_scenario_conflict_journey_AdministrationId1",
                table: "scenario",
                column: "AdministrationId1",
                principalTable: "conflict_journey",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_scenario_conflict_journey_ClimaxId1",
                table: "scenario",
                column: "ClimaxId1",
                principalTable: "conflict_journey",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_scenario_conflict_journey_ConsequenceId1",
                table: "scenario",
                column: "ConsequenceId1",
                principalTable: "conflict_journey",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_scenario_conflict_journey_ContextId1",
                table: "scenario",
                column: "ContextId1",
                principalTable: "conflict_journey",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_scenario_conflict_journey_PerceptionId1",
                table: "scenario",
                column: "PerceptionId1",
                principalTable: "conflict_journey",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_scenario_stakeholder_stakeholder_relator_id",
                table: "scenario",
                column: "stakeholder_relator_id",
                principalTable: "stakeholder",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);


            //------------------------------------------//
            // Seed data

            //migrationBuilder.Sql(@"INSERT INTO conflict_type (type)
            //                       VALUES 
            //                        ('Project Priorities'),
            //                        ('Administrative Procedures'),
            //                        ('Technical Matters'),
            //                        ('Costs'),
            //                        ('Schedule'),
            //                        ('Personality'),
            //                        ('Responsibilities'),
            //                        ('Human Resources'),
            //                        ('Equipment and Facilities')");

            //migrationBuilder.Sql(@"INSERT INTO severity (type, level)
            //                       VALUES 
            //                        ('Premise accepts no criticism, no interaction between team members', 1),
            //                        ('Deal with smoothness and harmony after a brief discussion', 2),
            //                        ('Long period of constructive debate, discussing the virtues of a problem', 3),
            //                        ('Caused a slight interruption by forcing people to solve problems relevant', 4),
            //                        ('Long period of destructive debate resulting in wasted time for get back on track', 5),
            //                        ('Complete interruption of team work', 6)");

            //migrationBuilder.Sql(@"INSERT INTO manage_conflict (style, description, effect)
            //                       VALUES 
            //                        ('Withdrawal / Avoidance', 'Withdrawal from a current or potential conflict situation', 'It does not solve the problem'),
            //                        ('Smoothing / Accommodation', 'Enfatiza as áreas de acordo em vez das áreas de diferença', 'Provides short solution only term'),
            //                        ('Commitment', 'Search and negotiate solutions that bring some degree of satisfaction to all parts', 'Provides definitive resolution'),
            //                        ('Force', 'Boosts the point of view of someone over others; It only offers situations of win-lose', 'Grudges may come back in other ways'),
            //                        ('Collaboration', 'It incorporates various points of view and ideas from different perspectives; It leads to consensus and commitment', 'Provides long term solution'),
            //                        ('Clash / Resolution Problem', 'Treats conflict as a problem to be solved by examining alternatives; Requires attitude of exchange and open dialogue', 'Provides final resolution')");

            //------------------------------------------//

            migrationBuilder.Sql(@"INSERT INTO users (username,password, role)
                                   VALUES 
                                    ('admin', '$2b$10$3WHy2Jc/1UlZx1yqql2hMuVtjUWX8/7k8yi2HAC4.4rWZEMWlpCtC', 1)");


            migrationBuilder.Sql(@"INSERT INTO conflict_type (type, user_id)
                                   VALUES 
                                    ('Prioridades do projeto', 1),
                                    ('Procedimentos administrativos', 1),
                                    ('Assuntos técnicos', 1),
                                    ('Custos', 1),
                                    ('Cronogramas', 1),
                                    ('Personalidade', 1),
                                    ('Responsabilidades', 1),
                                    ('Recursos humanos', 1),
                                    ('Equipamentos e instalações', 1)");

            migrationBuilder.Sql(@"INSERT INTO severity (type, level, user_id)
                                   VALUES 
                                    ('Premissa aceita sem críticas, sem interação entre os membros da equipe', 1, 1),
                                    ('Lidou com suavidade e harmonia após uma breve discussão', 2, 1),
                                    ('Longo período de debate construtivo, discutindo as virtudes de um problema', 3, 1),
                                    ('Causou uma pequena interrupção ao forçar as pessoas a resolver problemas relevantes', 4, 1),
                                    ('Longo período de debates destrutivos, resultando em perda de muito tempo para voltar aos trilhos', 5, 1),
                                    ('Causou interrupção completa no trabalho da equipe', 6, 1)");

            migrationBuilder.Sql(@"INSERT INTO manage_conflict (style, description, effect, user_id)
                                   VALUES 
                                    ('Retirada / Evitação', 'Retira de uma atual ou potencial situação de conflito', 'Não resolve o problema', 1),
                                    ('Suavização / Acomodação', 'Enfatiza as áreas de acordo em vez das áreas de diferença', 'Fornece apenas solução de curto prazo', 1),
                                    ('Comprometimento', 'Procura e negocia soluções que tragam algum grau de satisfação a todas as partes', 'Fornece resolução definitiva', 1),
                                    ('Forçar', 'Impulsiona o ponto de vista de alguém em detrimento de outros; Oferece apenas situações de ganha-perde', 'Os ressentimentos podem voltar de outras formas', 1),
                                    ('Colaboração', 'Incorpora vários pontos de vista e idéias de diferentes perspectivas; leva ao consenso e compromisso', 'Fornece solução de longo prazo', 1),
                                    ('Confronto / Resolução de Problema', 'Trata o conflito como um problema a ser resolvido examinando alternativas; Requer atitude de troca e diálogo aberto', 'Fornece resolução final', 1)");



        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_company_users_user_id",
                table: "company");

            migrationBuilder.DropForeignKey(
                name: "FK_conflict_journey_users_UserId",
                table: "conflict_journey");

            migrationBuilder.DropForeignKey(
                name: "FK_interview_users_user_id",
                table: "interview");

            migrationBuilder.DropForeignKey(
                name: "FK_scenario_users_user_id",
                table: "scenario");

            migrationBuilder.DropForeignKey(
                name: "FK_severity_users_user_id",
                table: "severity");

            migrationBuilder.DropForeignKey(
                name: "FK_stakeholder_users_user_id",
                table: "stakeholder");

            migrationBuilder.DropForeignKey(
                name: "FK_conflict_journey_scenario_ScenarioId1",
                table: "conflict_journey");

            migrationBuilder.DropForeignKey(
                name: "FK_stakeholder_scenario_ScenarioId1",
                table: "stakeholder");

            migrationBuilder.DropTable(
                name: "scenario_conflict_type");

            migrationBuilder.DropTable(
                name: "scenario_effect_consequence");

            migrationBuilder.DropTable(
                name: "scenario_manage_conflict");

            migrationBuilder.DropTable(
                name: "scenario_se_phase");

            migrationBuilder.DropTable(
                name: "stakeholder_empathy_map");

            migrationBuilder.DropTable(
                name: "conflict_type");

            migrationBuilder.DropTable(
                name: "effect_consequence");

            migrationBuilder.DropTable(
                name: "manage_conflict");

            migrationBuilder.DropTable(
                name: "se_phase");

            migrationBuilder.DropTable(
                name: "empathy_map");

            migrationBuilder.DropTable(
                name: "users");

            migrationBuilder.DropTable(
                name: "scenario");

            migrationBuilder.DropTable(
                name: "conflict_journey");

            migrationBuilder.DropTable(
                name: "company");

            migrationBuilder.DropTable(
                name: "interview");

            migrationBuilder.DropTable(
                name: "severity");

            migrationBuilder.DropTable(
                name: "stakeholder");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAO.Migrations
{
    public partial class _4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "team_localization",
                table: "scenario",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "team_localization",
                table: "scenario",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}

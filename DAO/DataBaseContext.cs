﻿using DAO.Configurations;
using DAO.Configurations.Relationships;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace DAO
{
    public class DataBaseContext : DbContext
    {
        private IDbContextTransaction _dbContextTransaction;

        public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ScenarioConfiguration());
            modelBuilder.ApplyConfiguration(new ConflictTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ScenarioConflictTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SeverityConfiguration());
            modelBuilder.ApplyConfiguration(new StakeholderConfiguration());
            modelBuilder.ApplyConfiguration(new SEPhaseConfiguration());
            modelBuilder.ApplyConfiguration(new ScenarioSEPhaseConfiguration());
            modelBuilder.ApplyConfiguration(new ManageConflictConfiguration());
            modelBuilder.ApplyConfiguration(new ScenarioManageConflictConfiguration());
            modelBuilder.ApplyConfiguration(new EffectConsequenceConfiguration());
            modelBuilder.ApplyConfiguration(new ScenarioEffectConsequenceConfiguration());
            modelBuilder.ApplyConfiguration(new CompanyConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new InterviewConfiguration());
            modelBuilder.ApplyConfiguration(new EmpathyMapConfiguration());
            modelBuilder.ApplyConfiguration(new StakeholderEmpathyMapConfiguration());
            modelBuilder.ApplyConfiguration(new ConflictJourneyConfiguration());
            
        }

        public DbSet<Scenario> Scenario { get; set; }

        public new void SaveChanges()
        {
            base.SaveChanges();
        }

        public new DbSet<T> Set<T>() where T : class
        {
            return base.Set<T>();
        }

        public void BeginTransaction()

        {
            _dbContextTransaction = Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            if (_dbContextTransaction != null)
            {
                _dbContextTransaction.Commit();
            }
        }

        public void RollbackTransaction()
        {
            if (_dbContextTransaction != null)

            {
                _dbContextTransaction.Rollback();
            }
        }

        public void DisposeTransaction()

        {

            if (_dbContextTransaction != null)
            {
                _dbContextTransaction.Dispose();
            }
        }
    }
}

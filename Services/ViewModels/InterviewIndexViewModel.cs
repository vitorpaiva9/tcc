﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class InterviewIndexViewModel
    {
        public List<InterviewViewModel> InterviewList { get; set; }
        public bool IsAdmin { get; set; }
        public int CurrentUserId { get; set; }
    }
}

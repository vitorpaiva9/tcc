﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Services.ViewModels
{
    public class UserViewModel : BaseViewModel
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        //[Display(Name = "User Name")]
        [Display(Name = "Nome do Usuário")]
        public string Username { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [Display(Name = "Senha")]

        public string Password { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [EmailAddress]
        [Display(Name = "E-mail")]
        [Compare(nameof(ConfirmEmail))]
        public string Email { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [EmailAddress]
        //[Display(Name = "Confirm E-mail")]
        [Display(Name = "Confirme o E-mail")]
        public string ConfirmEmail { get; set; }
        [Display(Name = "Papel")]
        public int Role { get; set; }
    }
}

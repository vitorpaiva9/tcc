﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Services.ViewModels
{
    public class StakeholderViewModel : BaseViewModel
    {
        public StakeholderViewModel()
        {
            Think = new EmpathyMapViewModel() {EmpathyMapType = EmpathyMapType.Think };
            See = new EmpathyMapViewModel() {EmpathyMapType = EmpathyMapType.See };
            Do = new EmpathyMapViewModel() {EmpathyMapType = EmpathyMapType.Do };
            Speak = new EmpathyMapViewModel() {EmpathyMapType = EmpathyMapType.Speak };
            Listen = new EmpathyMapViewModel() {EmpathyMapType = EmpathyMapType.Listen };
            Feel = new EmpathyMapViewModel() {EmpathyMapType = EmpathyMapType.Feel };
            Pain = new EmpathyMapViewModel() {EmpathyMapType = EmpathyMapType.Pain };
            Need = new EmpathyMapViewModel() {EmpathyMapType = EmpathyMapType.Need };
        }

        //[Display(Name = "Name")]
        [Display(Name = "Personalidade")]
        public StakeholderPersonality Description { get; set; }

        //[Display(Name = "Type")]
        [Display(Name = "Tipo de Stakeholder")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public StakeholderType Type { get; set; }



        //[Display(Name = "Is Relator")]
        [Display(Name = "É Relator")]
        public bool IsRelator { get; set; }

        [Display(Name = "O que Pensa?")]
        public EmpathyMapViewModel Think { get; set; }
        [Display(Name = "O que Vê?")]
        public EmpathyMapViewModel See { get; set; }
        [Display(Name = "O que Faz?")]
        public EmpathyMapViewModel Do { get; set; }
        [Display(Name = "O que Fala?")]
        public EmpathyMapViewModel Speak { get; set; }
        [Display(Name = "O que Escuta?")]
        public EmpathyMapViewModel Listen { get; set; }
        [Display(Name = "O que Sente?")]
        public EmpathyMapViewModel Feel { get; set; }
        [Display(Name = "Quais as Dores?")]
        public EmpathyMapViewModel Pain { get; set; }
        [Display(Name = "Quais as Necessidades?")]
        public EmpathyMapViewModel Need { get; set; }

        public int? UserId { get; set; }
        public ScenarioViewModel Scenario { get; set; }
        public int ScenarioId { get; set; }

    }
}

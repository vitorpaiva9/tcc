﻿using System.ComponentModel.DataAnnotations;

namespace Services.ViewModels
{
    public class ConflictTypeViewModel : BaseViewModel
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        //[Display(Name = "Type")]
        [Display(Name = "Tipo do Conflito")]
        public string Type { get; set; }
        public int? UserId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class ManageConflictIndexViewModel
    {
        public List<ManageConflictViewModel> ManageConflictList { get; set; }
        public bool IsAdmin { get; set; }
        public int CurrentUserId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class EffectConsequenceIndexViewModel
    {
        public List<EffectConsequenceViewModel> EffectConsequenceList { get; set; }
        public bool IsAdmin { get; set; }
        public int CurrentUserId { get; set; }
    }
}

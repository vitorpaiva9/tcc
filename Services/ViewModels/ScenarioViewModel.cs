﻿using Domain.Enums;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Services.ViewModels
{

    public class ScenarioViewModel : BaseViewModel
    {
        public ScenarioViewModel()
        {
            ScenarioGeneralViewModel = new ScenarioGeneralViewModel();
            ScenarioStakeholdersViewModel = new ScenarioStakeholdersViewModel();
            ScenarioConflictViewModel = new ScenarioConflictViewModel();
            ScenarioConflictJourneyViewModel = new ScenarioConflictJourneyViewModel();
        }


        public ScenarioGeneralViewModel ScenarioGeneralViewModel { get; set; }
        public ScenarioStakeholdersViewModel ScenarioStakeholdersViewModel { get; set; }
        public ScenarioConflictViewModel ScenarioConflictViewModel { get; set; }
        public ScenarioConflictJourneyViewModel ScenarioConflictJourneyViewModel { get; set; }

    }
}

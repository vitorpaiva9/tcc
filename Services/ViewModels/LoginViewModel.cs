﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Services.ViewModels
{
    public class LoginViewModel
    {
        //[Display(Name = "User Name")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [Display(Name = "Usuário")]
        public string Username { get; set; }
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [Display(Name = "Senha")]
        public string Password { get; set; }

    }
}

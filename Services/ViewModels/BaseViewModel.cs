﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class BaseViewModel
    {
        public int Id { get; set; }

        public bool FromModal { get; set; }
    }
}

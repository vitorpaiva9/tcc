﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Services.ViewModels
{
    public class CompanyViewModel : BaseViewModel
    {
        [Display(Name = "Descrição")]

        public string Description { get; set; }

        [Display(Name = "Pública ou Privada")]
        public PublicOrPrivate PublicOrPrivate { get; set; }

        [Display(Name = "Perfil de Empresa")]
        public string Type { get; set; }

        [Display(Name = "Número de Funcionários")]
        public int EmployeesNumber { get; set; }
        public int? UserId { get; set; }
    }
}

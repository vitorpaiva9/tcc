﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class SEPhaseIndexViewModel
    {
        public List<SEPhaseViewModel> SEPhaseList { get; set; }
        public bool IsAdmin { get; set; }
        public int CurrentUserId { get; set; }
    }
}

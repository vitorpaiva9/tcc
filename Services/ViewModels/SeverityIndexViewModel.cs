﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class SeverityIndexViewModel
    {
        public List<SeverityViewModel> SeverityList { get; set; }
        public bool IsAdmin { get; set; }
        public int CurrentUserId { get; set; }
    }
}

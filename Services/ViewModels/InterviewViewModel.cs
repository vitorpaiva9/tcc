﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Services.ViewModels
{
    public class InterviewViewModel : BaseViewModel
    {
        public InterviewViewModel()
        {
            Realization = DateTime.Today;
        }
        [Display(Name = "Título")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public string Title { get; set; }

        [Display(Name = "Entrevistador")]

        public string Interviewer { get; set; }

        [Display(Name = "Data da Realização")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public DateTime Realization { get; set; }

        public int? UserId { get; set; }
    }
}

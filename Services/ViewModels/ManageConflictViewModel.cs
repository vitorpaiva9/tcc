﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Services.ViewModels
{
    public class ManageConflictViewModel : BaseViewModel
    {
        [Display(Name = "Administração do Conflito")]
        public string Style { get; set; }
        [Display(Name = "Descrição")]
        public string Description { get; set; }
        [Display(Name = "Efeito")]
        public string Effect { get; set; }
        public int? UserId { get; set; }
    }
}

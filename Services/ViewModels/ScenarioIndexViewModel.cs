﻿using Domain.Enums;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Services.ViewModels
{
    public class ScenarioIndexViewModel
    {
        public ScenarioIndexViewModel()
        {
            ConflictTypeList = new List<ConflictTypeViewModel>();
            SeverityList = new List<SeverityViewModel>();
            SEPhaseList = new List<SEPhaseViewModel>();
            ManageConflictList = new List<ManageConflictViewModel>();

        }

        [Display(Name = "Search field")]
        public string Search { get; set; }

        [Display(Name = "Status do Cenário")]
        public Status? Status { get; set; }

        public IEnumerable<SeverityViewModel> SeverityList { get; set; }

        public SelectList SeveritySelectList
        {
            get
            {
                return new SelectList(SeverityList, nameof(SeverityViewModel.Id), nameof(SeverityViewModel.Type), 0);
            }
        }
        //[Display(Name = "Severity Level")]
        [Display(Name = "Grau de Severidade")]
        public int? SeverityId { get; set; }

        [Display(Name = "Impacto do Conflito")]
        public ConflictImpact? ConflictImpact { get; set; }


        public IEnumerable<SEPhaseViewModel> SEPhaseList { get; set; }

        public SelectList SEPhaseSelectList
        {
            get
            {
                return new SelectList(SEPhaseList, nameof(SEPhaseViewModel.Id), nameof(SEPhaseViewModel.Phase), 0);
            }
        }
        [Display(Name = "Fase(s) da Engenharia de Software")]
        public IEnumerable<int> SEPhaseIds { get; set; }



        public IEnumerable<ManageConflictViewModel> ManageConflictList { get; set; }

        public SelectList ManageConflictSelectList
        {
            get
            {
                return new SelectList(ManageConflictList, nameof(ManageConflictViewModel.Id), nameof(ManageConflictViewModel.Style), 0);
            }
        }
        //[Display(Name = "Conflict Management Style Applied")]
        [Display(Name = "Administração de Conflito")]
        public IEnumerable<int> ManageConflictIds { get; set; }

        //-----------------------------------------------------


        public bool IsAdmin { get; set; }
        public int CurrentUserId { get; set; }
        public IEnumerable<ScenarioViewModel> ScenarioViewModelList { get; set; }

        public IEnumerable<ConflictTypeViewModel> ConflictTypeList { get; set; }




    }
}

﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Services.ViewModels
{
    public class ScenarioConflictJourneyViewModel : BaseViewModel
    {
        public ScenarioConflictJourneyViewModel()
        {
            Evolutions = new List<ConflictJourneyViewModel>() { new ConflictJourneyViewModel() };
            EffectConsequenceList = new List<EffectConsequenceViewModel>();

        }

        public ConflictJourneyViewModel Context { get; set; }
        public ConflictJourneyViewModel Perception { get; set; }
        public List<ConflictJourneyViewModel> Evolutions { get; set; }
        public ConflictJourneyViewModel Climax { get; set; }
        public ConflictJourneyViewModel Administration { get; set; }
        public ConflictJourneyViewModel Consequence { get; set; }

        public bool IsDetails { get; set; }

        //

        public IEnumerable<EffectConsequenceViewModel> EffectConsequenceList { get; set; }

        public SelectList EffectConsequenceSelectList
        {
            get
            {
                return new SelectList(EffectConsequenceList, nameof(EffectConsequenceViewModel.Id), nameof(EffectConsequenceViewModel.EffectAndConsequence), 0);
            }
        }
        //[Display(Name = "Effect and Consequences")]
        [Display(Name = "Efeitos e Consequências")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public IEnumerable<int> EffectConsequenceIds { get; set; }

        [Display(Name = "Descrição")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public string DescriptionStakeholder1 { get; set; }
        [Display(Name = "Descrição")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public string DescriptionStakeholder2 { get; set; }
    }
}

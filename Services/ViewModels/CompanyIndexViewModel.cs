﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class CompanyIndexViewModel
    {
        public List<CompanyViewModel> CompanyList { get; set; }
        public bool IsAdmin { get; set; }
        public int CurrentUserId { get; set; }
    }
}

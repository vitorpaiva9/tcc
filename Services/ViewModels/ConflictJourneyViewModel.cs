﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class ConflictJourneyViewModel
    {
        public Feelings FeelingStakeholder1 { get; set; }
        public Behavior BehaviorStakeholder1 { get; set; }

        public string Stage { get; set; }

        public Feelings FeelingStakeholder2 { get; set; }
        public Behavior BehaviorStakeholder2 { get; set; }
    }
}

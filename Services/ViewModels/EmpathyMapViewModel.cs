﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Services.ViewModels
{
    public class EmpathyMapViewModel : BaseViewModel
    {
        public EmpathyMapType EmpathyMapType { get; set; }

        [Display(Name = "Descrição")]
        public string Description { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Services.ViewModels
{
    public class EffectConsequenceViewModel : BaseViewModel
    {
        [Display(Name = "Efeitos e Consequências")]
        public string EffectAndConsequence { get; set; }

        public int? UserId { get; set; }
    }
}

﻿using Domain.Enums;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Services.ViewModels
{
    public class ScenarioConflictViewModel : BaseViewModel
    {
        public ScenarioConflictViewModel()
        {
            ConflictTypeList = new List<ConflictTypeViewModel>();
            SeverityList = new List<SeverityViewModel>();
            SEPhaseList = new List<SEPhaseViewModel>();
            ManageConflictList = new List<ManageConflictViewModel>();
        }

        //---------

        public IEnumerable<SeverityViewModel> SeverityList { get; set; }

        public SelectList SeveritySelectList
        {
            get
            {
                return new SelectList(SeverityList, nameof(SeverityViewModel.Id), nameof(SeverityViewModel.Type), 0);
            }
        }
        //[Display(Name = "Severity Level")]
        [Display(Name = "Grau de Severidade")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        //[Range(1, 99999, ErrorMessage = "The Severity Level field is required.")]
        [Range(1, 99999, ErrorMessage = "O Grau de Severidade é obrigatório.")]
        public int? SeverityId { get; set; }


        //---------

        public IEnumerable<ConflictTypeViewModel> ConflictTypeList { get; set; }

        public SelectList ConflictTypeSelectList
        {
            get
            {
                return new SelectList(ConflictTypeList, nameof(ConflictTypeViewModel.Id), nameof(ConflictTypeViewModel.Type), 0);
            }
        }
        //[Display(Name = "Type of Conflicts")]
        [Display(Name = "Tipo(s) de Conflito")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public IEnumerable<int> ConflictTypeIds { get; set; }

        //---------

        public IEnumerable<SEPhaseViewModel> SEPhaseList { get; set; }

        public SelectList SEPhaseSelectList
        {
            get
            {
                return new SelectList(SEPhaseList, nameof(SEPhaseViewModel.Id), nameof(SEPhaseViewModel.Phase), 0);
            }
        }
        //[Display(Name = "Software Engineer Phases")]
        [Display(Name = "Fase(s) da Engenharia de Software")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public IEnumerable<int> SEPhaseIds { get; set; }

        //---------

        public IEnumerable<ManageConflictViewModel> ManageConflictList { get; set; }

        public SelectList ManageConflictSelectList
        {
            get
            {
                return new SelectList(ManageConflictList, nameof(ManageConflictViewModel.Id), nameof(ManageConflictViewModel.Style), 0);
            }
        }
        //[Display(Name = "Conflict Management Style Applied")]
        [Display(Name = "Administração de Conflito")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public IEnumerable<int> ManageConflictIds { get; set; }

        //---------

        [Display(Name = "Tipo de Desenvolvimento")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public DevelopingType DevelopingType { get; set; }

        [Display(Name = "Impacto do Conflito")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public ConflictImpact ConflictImpact { get; set; }

        [Display(Name = "Distribuição da Equipe")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public string TeamLocalization { get; set; }

        public bool IsDetails { get; set; }

        public bool HasErrorConflict { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Services.ViewModels
{
    public class SEPhaseViewModel : BaseViewModel
    {
        [Display(Name = "Fase da Engenharia de Software")]
        public string Phase { get; set; }
        public int? UserId { get; set; }
    }
}

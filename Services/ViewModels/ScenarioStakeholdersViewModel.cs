﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Services.ViewModels
{
    public class ScenarioStakeholdersViewModel : BaseViewModel
    {
        public ScenarioStakeholdersViewModel()
        {
            Stakeholders = new List<StakeholderViewModel>() { new StakeholderViewModel(), new StakeholderViewModel() };
            RelatorIndex = new List<int?>();
        }
        public List<StakeholderViewModel> Stakeholders { get; set; }


        //[Display(Name = "Relator of the Scenario?")]
        [Display(Name = "Relator do Cenário?")]
        public List<int?> RelatorIndex { get; set; }

        public int? StakeholderRelatorId { get; set; }

        public bool IsDetails { get; set; }

        public bool HasErrorStakeholders { get; set; }

    }
}

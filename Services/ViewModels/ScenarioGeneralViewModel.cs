﻿using Domain.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Services.ViewModels
{
    public class ScenarioGeneralViewModel : BaseViewModel
    {
        public ScenarioGeneralViewModel()
        {
            CompanyList = new List<CompanyViewModel>();
            InterviewList = new List<InterviewViewModel>();

        }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        //[Display(Name = "Summary")]
        [Display(Name = "Resumo")]
        public string Summary { get; set; }

        //[Display(Name = "Problem Summary")]
        [Display(Name = "Problema")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public string ProblemSummary { get; set; }

        //[Display(Name = "Origin of the Scenario")]
        [Display(Name = "Origem do Cenário")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public ScenarioOrigin ScenarioOrigin { get; set; }

        [Display(Name = "Status do Cenário")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public Status? Status { get; set; }



        //---------

        public IEnumerable<InterviewViewModel> InterviewList { get; set; }

        public SelectList InterviewSelectList
        {
            get
            {
                return new SelectList(InterviewList, nameof(InterviewViewModel.Id), nameof(InterviewViewModel.Title), 0);
            }
        }
        //[Display(Name = "Interview")]
        [Display(Name = "Entrevista")]
        //[Required(ErrorMessage = "O campo {0} é obrigatório.")]
        //[Range(1, 99999, ErrorMessage = "The Interview field is required.")]
        public int? InterviewId { get; set; }
        //---------

        public IEnumerable<CompanyViewModel> CompanyList { get; set; }

        public SelectList CompanySelectList
        {
            get
            {
                return new SelectList(CompanyList, nameof(CompanyViewModel.Id), nameof(CompanyViewModel.Type), 0);
            }
        }
        //[Display(Name = "Company")]
        [Display(Name = "Perfil de Empresa")]
        public int? CompanyId { get; set; }

        [Display(Name = @"Arquivo da Transcrição (Apenas '.txt', '.doc' e '.docx' )")]

        public IFormFile TranscriptionFile { get; set; }

        //[Display(Name = "Transcription")]
        [Display(Name = "Transcrição")]
        //[Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public string TranscriptionPath { get; set; }


        [Display(Name = @"Áudio da entrevista (Apenas '.mp3', '.wma' e '.aac' )")]

        public IFormFile AudioFile { get; set; }

        //[Display(Name = "Transcription")]
        [Display(Name = "Audio")]
        //[Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public string AudioPath { get; set; }


        [Display(Name = "Nível do Conflito")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public Level Level { get; set; }

        [Display(Name = "Origem do Conflito")]
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public Origin Origin { get; set; }

        public int? UserId { get; set; }

        public bool IsDetails { get; set; }

        public bool HasError { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Services.ViewModels
{
     public class SeverityViewModel : BaseViewModel
    {
        [Display(Name = "Grau de Severidade")]
        public string Type { get; set; }
        [Display(Name = "Nível de Severidade")]
        public int Level { get; set; }
        public int? UserId { get; set; }
    }
}

﻿using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Services.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Services.AutoMapper.Mappers
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<ConflictType, ConflictTypeViewModel>().ReverseMap();
            CreateMap<Severity, SeverityViewModel>().ReverseMap();
            CreateMap<ConflictJourney, ConflictJourneyViewModel>().ReverseMap();

            CreateMap<EmpathyMap, EmpathyMapViewModel>();
            CreateMap<EmpathyMapViewModel, EmpathyMap>()
                .ForMember(dest => dest.StakeholderEmpathyMap, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore()); 

            CreateMap<Stakeholder, StakeholderViewModel>()
                .ForMember(dest => dest.Think,
                    opt => opt.MapFrom(src => src.StakeholderEmpathyMap.FirstOrDefault(x => x.EmpathyMap.EmpathyMapType == EmpathyMapType.Think).EmpathyMap))
                .ForMember(dest => dest.See,
                    opt => opt.MapFrom(src => src.StakeholderEmpathyMap.FirstOrDefault(x => x.EmpathyMap.EmpathyMapType == EmpathyMapType.See).EmpathyMap))
                .ForMember(dest => dest.Do,
                    opt => opt.MapFrom(src => src.StakeholderEmpathyMap.FirstOrDefault(x => x.EmpathyMap.EmpathyMapType == EmpathyMapType.Do).EmpathyMap))
                .ForMember(dest => dest.Speak,
                    opt => opt.MapFrom(src => src.StakeholderEmpathyMap.FirstOrDefault(x => x.EmpathyMap.EmpathyMapType == EmpathyMapType.Speak).EmpathyMap))
                .ForMember(dest => dest.Listen,
                    opt => opt.MapFrom(src => src.StakeholderEmpathyMap.FirstOrDefault(x => x.EmpathyMap.EmpathyMapType == EmpathyMapType.Listen).EmpathyMap))
                .ForMember(dest => dest.Feel,
                    opt => opt.MapFrom(src => src.StakeholderEmpathyMap.FirstOrDefault(x => x.EmpathyMap.EmpathyMapType == EmpathyMapType.Feel).EmpathyMap))
                .ForMember(dest => dest.Pain,
                    opt => opt.MapFrom(src => src.StakeholderEmpathyMap.FirstOrDefault(x => x.EmpathyMap.EmpathyMapType == EmpathyMapType.Pain).EmpathyMap))
                .ForMember(dest => dest.Need,
                    opt => opt.MapFrom(src => src.StakeholderEmpathyMap.FirstOrDefault(x => x.EmpathyMap.EmpathyMapType == EmpathyMapType.Need).EmpathyMap));

            CreateMap<StakeholderViewModel, Stakeholder>()
                .ForMember(dest => dest.StakeholderEmpathyMap,
                    opt => opt.Ignore());

            CreateMap<SEPhase, SEPhaseViewModel>().ReverseMap();
            CreateMap<ManageConflict, ManageConflictViewModel>().ReverseMap();
            CreateMap<EffectConsequence, EffectConsequenceViewModel>().ReverseMap();
            CreateMap<Company, CompanyViewModel>().ReverseMap();
            CreateMap<User, UserViewModel>().ReverseMap();
            CreateMap<Interview, InterviewViewModel>().ReverseMap();


            CreateMap<Scenario, ScenarioGeneralViewModel>()
                .ReverseMap();

            CreateMap<Scenario, ScenarioStakeholdersViewModel>()
                .ForMember(dest => dest.Stakeholders,
                    opt => opt.MapFrom(src => src.Stakeholders.ToList()));

            CreateMap<ScenarioStakeholdersViewModel, Scenario>()
                .ForMember(dest => dest.Stakeholders,
                    opt => opt.MapFrom(src => src.Stakeholders));


            CreateMap<Scenario, ScenarioConflictViewModel>()
                .ForMember(dest => dest.ConflictTypeIds,
                    opt => opt.MapFrom(src => src.ScenarioConflictType.Select(x => x.ConflictTypeId)))
                .ForMember(dest => dest.SEPhaseIds,
                    opt => opt.MapFrom(src => src.ScenarioSEPhase.Select(x => x.SEPhaseId)))
                .ForMember(dest => dest.ManageConflictIds,
                    opt => opt.MapFrom(src => src.ScenarioManageConflict.Select(x => x.ManageConflictId)))
                .ReverseMap();

            CreateMap<Scenario, ScenarioConflictJourneyViewModel>()
                .ForMember(dest => dest.Evolutions,
                    opt => opt.MapFrom(src => src.Evolutions.ToList()))
                .ForMember(dest => dest.EffectConsequenceIds,
                    opt => opt.MapFrom(src => src.ScenarioEffectConsequence.Select(x => x.EffectConsequenceId)));

            CreateMap<ScenarioConflictJourneyViewModel, Scenario>()
                .ForMember(dest => dest.Evolutions,
                    opt => opt.MapFrom(src => src.Evolutions));

            CreateMap<Scenario, ScenarioViewModel>()
                .ForMember(dest => dest.ScenarioGeneralViewModel,
                    opt => opt.MapFrom(src => src))
                .ForMember(dest => dest.ScenarioStakeholdersViewModel,
                    opt => opt.MapFrom(src => src))
                .ForMember(dest => dest.ScenarioConflictViewModel,
                    opt => opt.MapFrom(src => src))
                .ForMember(dest => dest.ScenarioConflictJourneyViewModel,
                    opt => opt.MapFrom(src => src))
                .ReverseMap();





        }
    }
}

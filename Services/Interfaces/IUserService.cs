﻿using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface IUserService : IService<UserViewModel>
    {
        UserViewModel GetByUsername(string username);

        UserViewModel Login(LoginViewModel login);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface IService<T> where T : class
    {
        T Get(int id);
        T Add(T viewModel);
        void Remove(int id);
        T Update(T viewModel);
        IEnumerable<T> GetAll();
        bool CanRemove(int id);
    }
}

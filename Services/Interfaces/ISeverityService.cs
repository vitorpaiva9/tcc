﻿using Services.ViewModels;

namespace Services.Interfaces
{
    public interface ISeverityService : IService<SeverityViewModel>
    {
        
    }
}

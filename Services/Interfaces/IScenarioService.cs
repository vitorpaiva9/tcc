﻿using Services.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IScenarioService : IService<ScenarioViewModel>
    {
        IEnumerable<ScenarioViewModel> GetAll(int userId, bool isAdmin);

        Task<ScenarioGeneralViewModel> AddGeneral(ScenarioGeneralViewModel viewModel);

        ScenarioStakeholdersViewModel AddStakeholders(ScenarioStakeholdersViewModel viewModel);

        ScenarioConflictViewModel AddConflict(ScenarioConflictViewModel viewModel);

        ScenarioConflictJourneyViewModel AddConflictJourney(ScenarioConflictJourneyViewModel viewModel);

    }
}

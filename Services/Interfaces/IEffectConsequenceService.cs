﻿using Services.ViewModels;

namespace Services.Interfaces
{
    public interface IEffectConsequenceService : IService<EffectConsequenceViewModel>
    {
    }
}

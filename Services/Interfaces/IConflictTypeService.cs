﻿using Services.ViewModels;

namespace Services.Interfaces
{
    public interface IConflictTypeService : IService<ConflictTypeViewModel>
    {
    }
}

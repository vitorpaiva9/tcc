﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Entities;
using DAO.Interfaces;
using Services.Interfaces;
using Services.ViewModels;
using Domain.Entities.Relationships;
using System.Linq;
using System;
using Domain.Enums;
using System.Transactions;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Services.Services
{
    public class ScenarioService : IScenarioService
    {
        private IScenarioRepository _scenarioRepository;
        private IConflictTypeRepository _conflictTypeRepository;
        private ISeverityRepository _severityRepository;
        private ISEPhaseRepository _sePhaseRepository;
        private IManageConflictRepository _manageConflictRepository;
        private IEffectConsequenceRepository _effectConsequenceRepository;
        private ICompanyRepository _companyRepository;
        private IMapper _mapper;
        private IConfiguration _config;

        public ScenarioService(IScenarioRepository scenarioRepository, IConflictTypeRepository conflictTypeRepository,
            ISeverityRepository severityRepository, ISEPhaseRepository sePhaseRepository, IManageConflictRepository manageConflictRepository,
            IEffectConsequenceRepository effectConsequenceRepository, ICompanyRepository companyRepository, IMapper mapper,
            IConfiguration config)
        {
            _scenarioRepository = scenarioRepository;
            _conflictTypeRepository = conflictTypeRepository;
            _severityRepository = severityRepository;
            _sePhaseRepository = sePhaseRepository;
            _manageConflictRepository = manageConflictRepository;
            _effectConsequenceRepository = effectConsequenceRepository;
            _companyRepository = companyRepository;
            _mapper = mapper;
            _config = config;

        }

        public ScenarioViewModel Add(ScenarioViewModel scenarioViewModel)
        {
            var scenario = _mapper.Map<Scenario>(scenarioViewModel);

            scenario.ScenarioConflictType = new List<ScenarioConflictType>();
            scenario.ScenarioSEPhase = new List<ScenarioSEPhase>();
            scenario.ScenarioManageConflict = new List<ScenarioManageConflict>();
            scenario.ScenarioEffectConsequence = new List<ScenarioEffectConsequence>();
            scenario.Stakeholders = new List<Stakeholder>();


            foreach (var conflictTypeId in scenarioViewModel.ScenarioConflictViewModel.ConflictTypeIds)
            {
                var conflictType = _conflictTypeRepository.Get(conflictTypeId);
                scenario.ScenarioConflictType.Add(new ScenarioConflictType { ConflictType = conflictType, Scenario = scenario });
            }

            foreach (var sePhaseId in scenarioViewModel.ScenarioConflictViewModel.SEPhaseIds)
            {
                var sePhase = _sePhaseRepository.Get(sePhaseId);
                scenario.ScenarioSEPhase.Add(new ScenarioSEPhase { SEPhase = sePhase, Scenario = scenario });
            }

            foreach (var manageConflictId in scenarioViewModel.ScenarioConflictViewModel.ManageConflictIds)
            {
                var manageConflict = _manageConflictRepository.Get(manageConflictId);
                scenario.ScenarioManageConflict.Add(new ScenarioManageConflict { ManageConflict = manageConflict, Scenario = scenario });
            }

            foreach (var EffectConsequenceId in scenarioViewModel.ScenarioConflictJourneyViewModel.EffectConsequenceIds)
            {
                var effectConsequence = _effectConsequenceRepository.Get(EffectConsequenceId);
                scenario.ScenarioEffectConsequence.Add(new ScenarioEffectConsequence { EffectConsequence = effectConsequence, Scenario = scenario });
            }

            foreach (var stakeholderViewModel in scenarioViewModel.ScenarioStakeholdersViewModel.Stakeholders)
            {
                var stakeholder = _mapper.Map<Stakeholder>(stakeholderViewModel);
                stakeholder.Scenario = scenario;
                scenario.Stakeholders.Add(stakeholder);
            }

            scenario.Severity = _severityRepository.Get(scenario.SeverityId.Value);

            if (scenario.CompanyId != null)
            {
                scenario.Company = _companyRepository.Get(scenario.CompanyId.Value);
            }


            if (scenario.InterviewId == 0)
            {
                scenario.InterviewId = null;
            }


            var scenarioAdded = _scenarioRepository.Add(scenario);

            var indexStakeholderRelator = scenarioViewModel.ScenarioStakeholdersViewModel.RelatorIndex.FirstOrDefault();

            if (indexStakeholderRelator != null)
            {
                var stakeholderRelator = scenario.Stakeholders.ElementAt(indexStakeholderRelator.Value);

                scenario.StakeholderRelatorId = stakeholderRelator?.Id;

            }

            return _mapper.Map<ScenarioViewModel>(scenario);
        }

        public async Task<ScenarioGeneralViewModel> AddGeneral(ScenarioGeneralViewModel viewModel)
        {

            if (viewModel.ScenarioOrigin != ScenarioOrigin.Interview)
            {
                viewModel.TranscriptionPath = null;
                viewModel.AudioPath = null;
            }
            else if(viewModel.TranscriptionFile != null && viewModel.AudioFile != null)
            {

                var pathDirectory = Path.Combine(_config.GetValue<string>("FilesDirectory"), viewModel.Id.ToString());
                var transcriptionPathFile = Path.Combine(pathDirectory, viewModel.TranscriptionFile.FileName);
                var audioPathFile = Path.Combine(pathDirectory, viewModel.AudioFile.FileName);

                Directory.CreateDirectory(pathDirectory);

                using (var stream = new FileStream(transcriptionPathFile, FileMode.Create))
                {
                    await viewModel.TranscriptionFile.CopyToAsync(stream);
                }
                viewModel.TranscriptionPath = transcriptionPathFile;

                using (var stream = new FileStream(audioPathFile, FileMode.Create))
                {
                    await viewModel.AudioFile.CopyToAsync(stream);
                }
                viewModel.AudioPath = audioPathFile;
            }

            if (viewModel.CompanyId == 0)
            {
                viewModel.CompanyId = null;
            }

            if (viewModel.InterviewId == 0)
            {
                viewModel.InterviewId = null;
            }

            Scenario scenario = null;

            if (viewModel.Id == 0)
            {


                scenario = _mapper.Map<ScenarioGeneralViewModel, Scenario>(viewModel);

                _scenarioRepository.Add(scenario);
            }
            else
            {
                scenario = _scenarioRepository.Get(viewModel.Id);
                scenario = _mapper.Map(viewModel, scenario);
                _scenarioRepository.Update(scenario);

            }

            return _mapper.Map<ScenarioGeneralViewModel>(scenario);
        }

        public ScenarioStakeholdersViewModel AddStakeholders(ScenarioStakeholdersViewModel viewModel)
        {

            Scenario scenario = null;

            if (viewModel.Id != 0)
            {

                scenario = _scenarioRepository.Get(viewModel.Id);


                _mapper.Map(viewModel, scenario);

                var stakeholdersFromScenarioAfterMap = scenario.Stakeholders.ToList();


                for (int i = 0; i < stakeholdersFromScenarioAfterMap.Count; i++)
                {
                    stakeholdersFromScenarioAfterMap[i].Scenario = scenario;

                    if (stakeholdersFromScenarioAfterMap[i].StakeholderEmpathyMap == null || stakeholdersFromScenarioAfterMap[i].StakeholderEmpathyMap.Count == 0)
                    {
                        stakeholdersFromScenarioAfterMap[i].StakeholderEmpathyMap = new List<StakeholderEmpathyMap>();

                        stakeholdersFromScenarioAfterMap[i].StakeholderEmpathyMap.Add(new StakeholderEmpathyMap() {Stakeholder = stakeholdersFromScenarioAfterMap[i], EmpathyMap = _mapper.Map<EmpathyMap>(viewModel.Stakeholders[i].Think) });
                        stakeholdersFromScenarioAfterMap[i].StakeholderEmpathyMap.Add(new StakeholderEmpathyMap() {Stakeholder = stakeholdersFromScenarioAfterMap[i], EmpathyMap = _mapper.Map<EmpathyMap>(viewModel.Stakeholders[i].See) });
                        stakeholdersFromScenarioAfterMap[i].StakeholderEmpathyMap.Add(new StakeholderEmpathyMap() {Stakeholder = stakeholdersFromScenarioAfterMap[i], EmpathyMap = _mapper.Map<EmpathyMap>(viewModel.Stakeholders[i].Do) });
                        stakeholdersFromScenarioAfterMap[i].StakeholderEmpathyMap.Add(new StakeholderEmpathyMap() {Stakeholder = stakeholdersFromScenarioAfterMap[i], EmpathyMap = _mapper.Map<EmpathyMap>(viewModel.Stakeholders[i].Speak) });
                        stakeholdersFromScenarioAfterMap[i].StakeholderEmpathyMap.Add(new StakeholderEmpathyMap() {Stakeholder = stakeholdersFromScenarioAfterMap[i], EmpathyMap = _mapper.Map<EmpathyMap>(viewModel.Stakeholders[i].Listen) });
                        stakeholdersFromScenarioAfterMap[i].StakeholderEmpathyMap.Add(new StakeholderEmpathyMap() {Stakeholder = stakeholdersFromScenarioAfterMap[i], EmpathyMap = _mapper.Map<EmpathyMap>(viewModel.Stakeholders[i].Feel) });
                        stakeholdersFromScenarioAfterMap[i].StakeholderEmpathyMap.Add(new StakeholderEmpathyMap() {Stakeholder = stakeholdersFromScenarioAfterMap[i], EmpathyMap = _mapper.Map<EmpathyMap>(viewModel.Stakeholders[i].Pain) });
                        stakeholdersFromScenarioAfterMap[i].StakeholderEmpathyMap.Add(new StakeholderEmpathyMap() {Stakeholder = stakeholdersFromScenarioAfterMap[i], EmpathyMap = _mapper.Map<EmpathyMap>(viewModel.Stakeholders[i].Need) });


                    }
                }

                _scenarioRepository.Update(scenario);

                var indexStakeholderRelator = viewModel.RelatorIndex.FirstOrDefault();

                if (indexStakeholderRelator != null)
                {
                    var stakeholderRelator = scenario.Stakeholders.ElementAt(indexStakeholderRelator.Value);

                    scenario.StakeholderRelatorId = stakeholderRelator?.Id;

                    _scenarioRepository.Update(scenario);

                    var test = _scenarioRepository.Get(viewModel.Id);


                }
            }
            else
            {
                throw new Exception();

            }

            return _mapper.Map<ScenarioStakeholdersViewModel>(scenario);
        }

        public ScenarioConflictViewModel AddConflict(ScenarioConflictViewModel viewModel)
        {

            Scenario scenario = null;

            if (viewModel.Id != 0)
            {

                scenario = _scenarioRepository.Get(viewModel.Id);
                scenario = _mapper.Map(viewModel, scenario);

                scenario.ScenarioConflictType.Clear();
                foreach (var conflictTypeId in viewModel.ConflictTypeIds)
                {
                    var conflictType = _conflictTypeRepository.Get(conflictTypeId);
                    scenario.ScenarioConflictType.Add(new ScenarioConflictType { ConflictType = conflictType, Scenario = scenario });
                }

                scenario.ScenarioSEPhase.Clear();
                foreach (var sePhaseId in viewModel.SEPhaseIds)
                {
                    var sePhase = _sePhaseRepository.Get(sePhaseId);
                    scenario.ScenarioSEPhase.Add(new ScenarioSEPhase { SEPhase = sePhase, Scenario = scenario });
                }

                scenario.ScenarioManageConflict.Clear();
                foreach (var manageConflictId in viewModel.ManageConflictIds)
                {
                    var manageConflict = _manageConflictRepository.Get(manageConflictId);
                    scenario.ScenarioManageConflict.Add(new ScenarioManageConflict { ManageConflict = manageConflict, Scenario = scenario });
                }

                scenario.Severity = _severityRepository.Get(scenario.SeverityId.Value);

                _scenarioRepository.Update(scenario);


            }
            else
            {
                throw new Exception();

            }

            return _mapper.Map<ScenarioConflictViewModel>(scenario);

        }

        public ScenarioConflictJourneyViewModel AddConflictJourney(ScenarioConflictJourneyViewModel viewModel)
        {

            Scenario scenario = null;

            if (viewModel.Id != 0)
            {

                scenario = _scenarioRepository.Get(viewModel.Id);
                scenario = _mapper.Map(viewModel, scenario);

                scenario.ScenarioEffectConsequence.Clear();
                foreach (var EffectConsequenceId in viewModel.EffectConsequenceIds)
                {
                    var effectConsequence = _effectConsequenceRepository.Get(EffectConsequenceId);
                    scenario.ScenarioEffectConsequence.Add(new ScenarioEffectConsequence { EffectConsequence = effectConsequence, Scenario = scenario });
                }

                _scenarioRepository.Update(scenario);


            }
            else
            {
                throw new Exception();

            }

            return _mapper.Map<ScenarioConflictJourneyViewModel>(scenario);

        }

        public bool CanRemove(int id)
        {
            throw new System.NotImplementedException();
        }

        public ScenarioViewModel Get(int id)
        {
            var model = _scenarioRepository.Get(id);

            return _mapper.Map<ScenarioViewModel>(model);
        }

        public IEnumerable<ScenarioViewModel> GetAll()
        {
            return _mapper.Map<List<ScenarioViewModel>>(_scenarioRepository.GetAll());
        }

        public IEnumerable<ScenarioViewModel> GetAll(int userId, bool isAdmin)
        {
            if (isAdmin)
            {
                return _mapper.Map<List<ScenarioViewModel>>(_scenarioRepository.GetAll());
            }
            else
            {
                return _mapper.Map<List<ScenarioViewModel>>(_scenarioRepository.GetAll().Where(x => x.UserId == userId));
            }



        }

        public void Remove(int id)
        {
            try
            {
                using (var ts = new TransactionScope())
                {
                    var scenario = _scenarioRepository.Get(id);
                    var stakeholders = scenario.Stakeholders.ToList();
                    foreach (var item in stakeholders)
                    {
                        _scenarioRepository.RemoveStakeholder(item.Id);
                    }
                    //scenario.Stakeholders.Clear();
                    _scenarioRepository.Remove(id);
                    // do the stuff
                    ts.Complete();
                }

            }
            catch (Exception e)
            {

                throw;
            }
        }

        public ScenarioViewModel Update(ScenarioViewModel scenarioViewModel)
        {
            var scenario = _scenarioRepository.Get(scenarioViewModel.Id);
            _mapper.Map(scenarioViewModel, scenario);

            scenario.ScenarioConflictType = new List<ScenarioConflictType>();
            scenario.ScenarioSEPhase = new List<ScenarioSEPhase>();
            scenario.ScenarioManageConflict = new List<ScenarioManageConflict>();
            scenario.ScenarioEffectConsequence = new List<ScenarioEffectConsequence>();

            foreach (var conflictTypeId in scenarioViewModel.ScenarioConflictViewModel.ConflictTypeIds)
            {
                var conflictType = _conflictTypeRepository.Get(conflictTypeId);
                scenario.ScenarioConflictType.Add(new ScenarioConflictType { ConflictType = conflictType, Scenario = scenario });
            }

            foreach (var sePhaseId in scenarioViewModel.ScenarioConflictViewModel.SEPhaseIds)
            {
                var sePhase = _sePhaseRepository.Get(sePhaseId);
                scenario.ScenarioSEPhase.Add(new ScenarioSEPhase { SEPhase = sePhase, Scenario = scenario });
            }

            foreach (var manageConflictId in scenarioViewModel.ScenarioConflictViewModel.ManageConflictIds)
            {
                var manageConflict = _manageConflictRepository.Get(manageConflictId);
                scenario.ScenarioManageConflict.Add(new ScenarioManageConflict { ManageConflict = manageConflict, Scenario = scenario });
            }

            foreach (var EffectConsequenceId in scenarioViewModel.ScenarioConflictJourneyViewModel.EffectConsequenceIds)
            {
                var effectConsequence = _effectConsequenceRepository.Get(EffectConsequenceId);
                scenario.ScenarioEffectConsequence.Add(new ScenarioEffectConsequence { EffectConsequence = effectConsequence, Scenario = scenario });
            }

            //verify later

            //scenario.Severity = _severityRepository.Get(scenario.SeverityId.Value);

            //if (scenario.CompanyId != null)
            //{
            //    scenario.Company = _companyRepository.Get(scenario.CompanyId.Value);
            //}


            //if (scenario.InterviewId == 0)
            //{
            //    scenario.InterviewId = null;
            //}


            //var scenarioAdded = _scenarioRepository.Update(scenario);

            //var indexStakeholderRelator = scenarioViewModel.RelatorIndex.FirstOrDefault();

            //if (indexStakeholderRelator != null)
            //{
            //    var stakeholderRelator = scenario.Stakeholders.ElementAt(indexStakeholderRelator.Value);

            //    scenario.StakeholderRelatorId = stakeholderRelator?.Id;

            //}


            _scenarioRepository.Update(scenario);

            return _mapper.Map<ScenarioViewModel>(scenario);
        }
    }
}

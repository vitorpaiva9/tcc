﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DAO.Interfaces;
using Domain.Entities;
using Services.Interfaces;
using Services.ViewModels;

namespace Services.Services
{
    public class ConflictTypeService : IConflictTypeService
    {
        private IScenarioRepository _scenarioRepository;
        private IConflictTypeRepository _conflictTypeRepository;
        private IMapper _mapper;

        public ConflictTypeService(IConflictTypeRepository conflictTypeRepository, IScenarioRepository scenarioRepository, IMapper mapper)
        {
            _conflictTypeRepository = conflictTypeRepository;
            _scenarioRepository = scenarioRepository;
            _mapper = mapper;
        }

        public ConflictTypeViewModel Add(ConflictTypeViewModel conflictTypeViewModel)
        {
            var conflictType = _mapper.Map<ConflictType>(conflictTypeViewModel);
            _conflictTypeRepository.Add(conflictType);
            

            return _mapper.Map<ConflictTypeViewModel>(conflictType);
        }

        public ConflictTypeViewModel Get(int id)
        {
            return _mapper.Map<ConflictTypeViewModel>(_conflictTypeRepository.Get(id));
        }

        public IEnumerable<ConflictTypeViewModel> GetAll()
        {
            return _mapper.Map<List<ConflictTypeViewModel>>(_conflictTypeRepository.GetAll());

        }
        public bool CanRemove(int id)
        {
            return _scenarioRepository.GetAll().Where(x => x.ScenarioConflictType.Where(y => y.ConflictTypeId == id).Count() > 0).Count() == 0;
        }
        public void Remove(int id)
        {
            _conflictTypeRepository.Remove(id);

        }

        public ConflictTypeViewModel Update(ConflictTypeViewModel conflictTypeViewModel)
        {
            var conflictType = _conflictTypeRepository.Get(conflictTypeViewModel.Id);
            _mapper.Map(conflictTypeViewModel, conflictType);
            _conflictTypeRepository.Update(conflictType);

            return _mapper.Map<ConflictTypeViewModel>(conflictType);
        }
    }
}

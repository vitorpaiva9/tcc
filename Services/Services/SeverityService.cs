﻿using AutoMapper;
using Domain.Entities;
using DAO.Interfaces;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Services.Services
{
    public class SeverityService : ISeverityService
    {

        private ISeverityRepository _severityRepository;
        private IScenarioRepository _scenarioRepository;
        private IMapper _mapper;

        public SeverityService(ISeverityRepository severityRepository, IScenarioRepository scenarioRepository, IMapper mapper)
        {
            _severityRepository = severityRepository;
            _scenarioRepository = scenarioRepository;
            _mapper = mapper;
        }

        public SeverityViewModel Add(SeverityViewModel viewModel)
        {
            var severity = _mapper.Map<Severity>(viewModel);
            _severityRepository.Add(severity);


            return _mapper.Map<SeverityViewModel>(severity);
        }

        public bool CanRemove(int id)
        {
            return _scenarioRepository.GetAll().Where(x => x.SeverityId == id).Count() == 0;
        }

        public SeverityViewModel Get(int id)
        {
            return _mapper.Map<SeverityViewModel>(_severityRepository.Get(id));

        }

        public IEnumerable<SeverityViewModel> GetAll()
        {
            return _mapper.Map<List<SeverityViewModel>>(_severityRepository.GetAll());

        }

        public void Remove(int id)
        {
            _severityRepository.Remove(id);

        }

        public SeverityViewModel Update(SeverityViewModel viewModel)
        {
            var severity = _severityRepository.Get(viewModel.Id);
            _mapper.Map(viewModel, severity);
            _severityRepository.Update(severity);

            return _mapper.Map<SeverityViewModel>(severity);
        }
    }
}

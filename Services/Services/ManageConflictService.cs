﻿using AutoMapper;
using DAO.Interfaces;
using Domain.Entities;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Services
{
    public class ManageConflictService : IManageConflictService
    {
        private IManageConflictRepository _manageConflictRepository;
        private IScenarioRepository _scenarioRepository;
        private IMapper _mapper;

        public ManageConflictService(IManageConflictRepository manageConflictRepository, IScenarioRepository scenarioRepository, IMapper mapper)
        {
            _manageConflictRepository = manageConflictRepository;
            _scenarioRepository = scenarioRepository;
            _mapper = mapper;
        }

        public ManageConflictViewModel Add(ManageConflictViewModel viewModel)
        {
            var manageConflict = _mapper.Map<ManageConflict>(viewModel);
            _manageConflictRepository.Add(manageConflict);


            return _mapper.Map<ManageConflictViewModel>(manageConflict);
        }

        public bool CanRemove(int id)
        {
            return _scenarioRepository.GetAll().Where(x => x.ScenarioManageConflict.Where(y => y.ManageConflictId == id).Count() > 0).Count() == 0;
        }

        public ManageConflictViewModel Get(int id)
        {
            return _mapper.Map<ManageConflictViewModel>(_manageConflictRepository.Get(id));
        }

        public IEnumerable<ManageConflictViewModel> GetAll()
        {
            return _mapper.Map<List<ManageConflictViewModel>>(_manageConflictRepository.GetAll());
        }

        public void Remove(int id)
        {
            _manageConflictRepository.Remove(id);
        }

        public ManageConflictViewModel Update(ManageConflictViewModel viewModel)
        {
            var manageConflict = _manageConflictRepository.Get(viewModel.Id);
            _mapper.Map(viewModel, manageConflict);
            _manageConflictRepository.Update(manageConflict);

            return _mapper.Map<ManageConflictViewModel>(manageConflict);
        }
    }
}

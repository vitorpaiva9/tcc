﻿using AutoMapper;
using DAO.Interfaces;
using Domain.Entities;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Services
{
    public class EffectConsequenceService : IEffectConsequenceService
    {
        private IEffectConsequenceRepository _effectConsequenceRepository;
        private IScenarioRepository _scenarioRepository;
        private IMapper _mapper;

        public EffectConsequenceService(IEffectConsequenceRepository effectConsequenceRepository, IScenarioRepository scenarioRepository, IMapper mapper)
        {
            _effectConsequenceRepository = effectConsequenceRepository;
            _scenarioRepository = scenarioRepository;
            _mapper = mapper;
        }
        public EffectConsequenceViewModel Add(EffectConsequenceViewModel viewModel)
        {
            var effectConsequence = _mapper.Map<EffectConsequence>(viewModel);
            _effectConsequenceRepository.Add(effectConsequence);


            return _mapper.Map<EffectConsequenceViewModel>(effectConsequence);
        }

        public EffectConsequenceViewModel Get(int id)
        {
            return _mapper.Map<EffectConsequenceViewModel>(_effectConsequenceRepository.Get(id));
        }

        public IEnumerable<EffectConsequenceViewModel> GetAll()
        {
            return _mapper.Map<List<EffectConsequenceViewModel>>(_effectConsequenceRepository.GetAll());

        }
        public bool CanRemove(int id)
        {
            return _scenarioRepository.GetAll().Where(x => x.ScenarioEffectConsequence.Where(y => y.EffectConsequenceId == id).Count() > 0).Count() == 0;
        }
        public void Remove(int id)
        {
            _effectConsequenceRepository.Remove(id);
        }

        public EffectConsequenceViewModel Update(EffectConsequenceViewModel viewModel)
        {
            var effectConsequence = _effectConsequenceRepository.Get(viewModel.Id);
            _mapper.Map(viewModel, effectConsequence);
            _effectConsequenceRepository.Update(effectConsequence);

            return _mapper.Map<EffectConsequenceViewModel>(effectConsequence);
        }
    }
}

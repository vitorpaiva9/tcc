﻿using AutoMapper;
using DAO.Interfaces;
using Domain.Entities;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Services
{
    public class SEPhaseService : ISEPhaseService
    {
        private ISEPhaseRepository _sePhaseRepository;
        private IScenarioRepository _scenarioRepository;
        private IMapper _mapper;

        public SEPhaseService(ISEPhaseRepository sePhaseRepository, IScenarioRepository scenarioRepository, IMapper mapper)
        {
            _sePhaseRepository = sePhaseRepository;
            _scenarioRepository = scenarioRepository;
            _mapper = mapper;
        }
        public SEPhaseViewModel Add(SEPhaseViewModel viewModel)
        {
            var sePhase = _mapper.Map<SEPhase>(viewModel);
            _sePhaseRepository.Add(sePhase);


            return _mapper.Map<SEPhaseViewModel>(sePhase);
        }

        public SEPhaseViewModel Get(int id)
        {
            return _mapper.Map<SEPhaseViewModel>(_sePhaseRepository.Get(id));
        }

        public IEnumerable<SEPhaseViewModel> GetAll()
        {
            return _mapper.Map<List<SEPhaseViewModel>>(_sePhaseRepository.GetAll());

        }
        public bool CanRemove(int id)
        {
            return _scenarioRepository.GetAll().Where(x => x.ScenarioSEPhase.Where(y => y.SEPhaseId == id).Count() > 0).Count() == 0;
        }
        public void Remove(int id)
        {
            _sePhaseRepository.Remove(id);
        }

        public SEPhaseViewModel Update(SEPhaseViewModel viewModel)
        {
            var sePhase = _sePhaseRepository.Get(viewModel.Id);
            _mapper.Map(viewModel, sePhase);
            _sePhaseRepository.Update(sePhase);

            return _mapper.Map<SEPhaseViewModel>(sePhase);
        }
    }
}

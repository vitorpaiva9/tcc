﻿using AutoMapper;
using Domain.Entities;
using DAO.Interfaces;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Services.Services
{
    public class InterviewService : IInterviewService
    {

        private IInterviewRepository _interviewRepository;
        private IScenarioRepository _scenarioRepository;
        private IMapper _mapper;

        public InterviewService(IInterviewRepository interviewRepository, IScenarioRepository scenarioRepository, IMapper mapper)
        {
            _interviewRepository = interviewRepository;
            _scenarioRepository = scenarioRepository;
            _mapper = mapper;
        }

        public InterviewViewModel Add(InterviewViewModel viewModel)
        {
            var interview = _mapper.Map<Interview>(viewModel);
            _interviewRepository.Add(interview);


            return _mapper.Map<InterviewViewModel>(interview);
        }

        public bool CanRemove(int id)
        {
            return _scenarioRepository.GetAll().Where(x => x.InterviewId == id).Count() == 0;
        }

        public InterviewViewModel Get(int id)
        {
            return _mapper.Map<InterviewViewModel>(_interviewRepository.Get(id));

        }

        public IEnumerable<InterviewViewModel> GetAll()
        {
            return _mapper.Map<List<InterviewViewModel>>(_interviewRepository.GetAll());

        }

        public void Remove(int id)
        {
            _interviewRepository.Remove(id);

        }

        public InterviewViewModel Update(InterviewViewModel viewModel)
        {
            var interview = _interviewRepository.Get(viewModel.Id);
            _mapper.Map(viewModel, interview);
            _interviewRepository.Update(interview);

            return _mapper.Map<InterviewViewModel>(interview);
        }
    }
}



﻿using AutoMapper;
using DAO.Interfaces;
using Domain.Entities;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Services
{
    public class CompanyService : ICompanyService
    {

        private ICompanyRepository _companyRepository;
        private IScenarioRepository _scenarioRepository;
        private IMapper _mapper;

        public CompanyService(ICompanyRepository companyRepository, IScenarioRepository scenarioRepository, IMapper mapper)
        {
            _companyRepository = companyRepository;
            _scenarioRepository = scenarioRepository;
            _mapper = mapper;
        }

        public CompanyViewModel Add(CompanyViewModel viewModel)
        {
            var company = _mapper.Map<Company>(viewModel);
            _companyRepository.Add(company);


            return _mapper.Map<CompanyViewModel>(company);
        }

        public bool CanRemove(int id)
        {
            return _scenarioRepository.GetAll().ToList().Where(x => x.CompanyId == id).Count() == 0;
        }

        public CompanyViewModel Get(int id)
        {
            return _mapper.Map<CompanyViewModel>(_companyRepository.Get(id));

        }

        public IEnumerable<CompanyViewModel> GetAll()
        {
            return _mapper.Map<List<CompanyViewModel>>(_companyRepository.GetAll());

        }

        public void Remove(int id)
        {
            _companyRepository.Remove(id);

        }

        public CompanyViewModel Update(CompanyViewModel viewModel)
        {
            var company = _companyRepository.Get(viewModel.Id);
            _mapper.Map(viewModel, company);
            _companyRepository.Update(company);

            return _mapper.Map<CompanyViewModel>(company);
        }
    }
}

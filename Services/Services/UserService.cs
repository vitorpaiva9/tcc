﻿using AutoMapper;
using DAO.Interfaces;
using Domain.Entities;
using Domain.Enums;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Services
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository;
        private IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public UserViewModel Add(UserViewModel viewModel)
        {
            User user = null;
            if (GetByUsername(viewModel.Username) == null)
            {
                viewModel.Password = BCrypt.Net.BCrypt.HashPassword(viewModel.Password);
                user = _mapper.Map<User>(viewModel);
                user.Role = (int)Roles.User;
                _userRepository.Add(user);

            }
            
            return _mapper.Map<UserViewModel>(user);
        }

        public bool CanRemove(int id)
        {
            throw new NotImplementedException();
        }

        public UserViewModel Get(int id)
        {
            return _mapper.Map<UserViewModel>(_userRepository.Get(id));

        }

        public IEnumerable<UserViewModel> GetAll()
        {
            return _mapper.Map<List<UserViewModel>>(_userRepository.GetAll());

        }


        public void Remove(int id)
        {
            _userRepository.Remove(id);

        }

        public UserViewModel Update(UserViewModel viewModel)
        {
            var user = _userRepository.Get(viewModel.Id);
            _mapper.Map(viewModel, user);
            _userRepository.Update(user);

            return _mapper.Map<UserViewModel>(user);
        }

        public UserViewModel GetByUsername(string username)
        {
            
            return _mapper.Map<UserViewModel>(_userRepository.GetAll().FirstOrDefault(x => x.Username.ToUpper().Equals(username.ToUpper())));

        }

        public UserViewModel Login(LoginViewModel login)
        {
            UserViewModel user = GetByUsername(login.Username);
            var p = BCrypt.Net.BCrypt.HashPassword(login.Password);
            if (user != null && BCrypt.Net.BCrypt.Verify(login.Password, user.Password))
            {

                return user;
            }
            else
            {
                return null;
            }

        }

    }
}
